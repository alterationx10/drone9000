package com.markslaboratory.drone9000;


import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.services.SensordroneService;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;

public class GreetingsFragment extends ServiceFragment {

    ImageView d9k;
    TextView greeting;
    String MAC;

    public GreetingsFragment(SensordroneService service) {
        super(service);
    }

    // We just handle connections here
    DroneEventHandler connectionHandler = new DroneEventHandler() {
        @Override
        public void parseEvent(DroneEventObject droneEventObject) {
            if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTED)) {
                // Connection now done in BG thread
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        greeting.setText("I'm completely operational,\nand all my circuits are functioning perfectly.");
                    }
                });
                droneService.sensorDrone.setLEDs(RED_HI, GREEN_HI, BLUE_HI);
                droneService.sensorDrone.measureBatteryVoltage();
            }
            else if (droneEventObject.matches(DroneEventObject.droneEventType.DISCONNECTED)) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        greeting.setText("Just what do you think you're doing?");
//                        ((MainActivity)getActivity()).btv.setText("Disconnected");
                    }
                });

            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        droneService.sensorDrone.registerDroneListener(connectionHandler);
        getActivity().setTitle("DRONE9000");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.greeting_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Clear all stored preferences (for testing)
//        prefEditor.clear();
//        prefEditor.commit();

        MAC = myPreferences.getString(PreferenceConstants.SENSORDRONE, "");

        greeting = (TextView)getActivity().findViewById(R.id.greeting_tv_greeting);
        d9k = (ImageView)getActivity().findViewById(R.id.greeting_iv_d9k);
        d9k.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (MAC.equals("")) {
                    greeting.setText("Although you took very thorough precautions in the settings configuring me, you do not have a Sensordrone paired.");
                    return true;
                }
                if (droneService.sensorDrone.isConnected) {
                    droneService.sensorDrone.setLEDs(0, 0, 0);
                    droneService.sensorDrone.disconnect();
                } else {
//                    droneService.sensorDrone.btConnect(MAC);
                    AsyncConnect bgConnector = new AsyncConnect();
                    bgConnector.execute(MAC);
                }

                return true;
            }
        });

        setLEDColors(R.color.menu_purple);

        if (!droneService.sensorDrone.isConnected) {
            greeting.setText("I'm afraid I can't let you do that...");
        } else {
            greeting.setText("I'm completely operational,\nand all my circuits are functioning perfectly.");
        }
        if (
                !MAC.equals("") // We have a stored Drone
                && !droneService.sensorDrone.isConnected // We're not already connected
                && myPreferences.getBoolean(PreferenceConstants.AUTO_CONNECT, false) // We say so.
                ) {
//            droneService.sensorDrone.btConnect(MAC);
            AsyncConnect bgConnector = new AsyncConnect();
            bgConnector.execute(MAC);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        droneService.sensorDrone.setLEDs(RED_HI, GREEN_HI, BLUE_HI);
    }

    @Override
    public void onPause() {
        super.onPause();
        droneService.sensorDrone.setLEDs(RED_HI, GREEN_HI, BLUE_HI);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        droneService.sensorDrone.unregisterDroneListener(connectionHandler);
    }

    private class AsyncConnect extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Let the user know we are trying to connect
            greeting.setText("I am attempting to connect...");
        }

        @Override
        protected Void doInBackground(String... params) {
            droneService.sensorDrone.btConnect(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // What if we didn't connect
            if (!droneService.sensorDrone.isConnected) {
                greeting.setText("My attempts to connect have failed.");
            }
        }
    }
}
