package com.markslaboratory.drone9000.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "adc_log")
public class ADCLog extends BaseLog  {

    @DatabaseField(columnName = "adc", canBeNull = false)
    private float adc;

    @DatabaseField(columnName = "voltage", canBeNull = false)
    private float voltage;

    public ADCLog() {
    }

    public float getAdc() {
        return adc;
    }

    public void setAdc(float adc) {
        this.adc = adc;
    }

    public float getVoltage() {
        return voltage;
    }

    public void setVoltage(float voltage) {
        this.voltage = voltage;
    }

    public ADCLog(float adc, float voltage) {
        setAdc(adc);
        setVoltage(voltage);
    }
}
