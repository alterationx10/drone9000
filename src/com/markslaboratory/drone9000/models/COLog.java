package com.markslaboratory.drone9000.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "co_log")
public class COLog extends BaseLog {

    @DatabaseField(columnName = "ppm_co", canBeNull = false)
    private float ppm;

    public COLog() {
    }

    public float getPpm() {
        return ppm;
    }

    public void setPpm(float ppm) {
        this.ppm = ppm;
    }

    public COLog(float ppm) {
        this.ppm = ppm;
    }
}
