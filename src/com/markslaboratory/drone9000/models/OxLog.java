package com.markslaboratory.drone9000.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "oxidizing_gas_log")
public class OxLog extends BaseLog  {

    @DatabaseField(columnName = "resistance", canBeNull = false)
    private float resistance;

    public OxLog() {
    }

    public float getResistance() {
        return resistance;
    }

    public void setResistance(float resistance) {
        this.resistance = resistance;
    }

    public OxLog(float resistance) {
        this.resistance = resistance;
    }
}
