package com.markslaboratory.drone9000.models;


import com.j256.ormlite.field.DatabaseField;

import java.util.Calendar;

public abstract class BaseLog {

    @DatabaseField(columnName = "id", generatedId = true)
    private int id;

    @DatabaseField(columnName = "timestamp_year", canBeNull = false)
    private int year;

    @DatabaseField(columnName = "timestamp_month", canBeNull = false)
    private int month;

    @DatabaseField(columnName = "timestamp_day", canBeNull = false)
    private int day;

    @DatabaseField(columnName = "timestamp_hour", canBeNull = false)
    private int hour;

    @DatabaseField(columnName = "timestamp_minute", canBeNull = false)
    private int minute;

    @DatabaseField(columnName = "timestamp_second", canBeNull = false)
    private int second;

    @DatabaseField(columnName = "timestamp_unix_time", canBeNull = false)
    private long unixTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public long getUnixTime() {
        return unixTime;
    }

    public void setUnixTime(long unixTime) {
        this.unixTime = unixTime;
    }

    public void setDate() {
        Calendar calendar = Calendar.getInstance();
        setYear(calendar.get(Calendar.YEAR));
        setMonth(calendar.get(Calendar.MONTH));
        setDay(calendar.get(Calendar.DAY_OF_MONTH));
        setHour(calendar.get(Calendar.HOUR_OF_DAY));
        setMinute(calendar.get(Calendar.MINUTE));
        setSecond(calendar.get(Calendar.SECOND));
        setUnixTime(calendar.getTimeInMillis());
    }

    public BaseLog() {
        // Always get date on create... Can call again to update it...
        setDate();
    }
}
