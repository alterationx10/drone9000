package com.markslaboratory.drone9000.models;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "ir_log")
public class IRLog extends BaseLog {

    @DatabaseField(columnName = "temperature", canBeNull = false)
    private float temperature;

    public IRLog() {
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public IRLog(float temperature) {
        this.temperature = temperature;
    }
}
