package com.markslaboratory.drone9000.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "rgbc_log")
public class RGBCLog extends BaseLog {

    @DatabaseField(columnName = "red", canBeNull = false)
    private float red;

    @DatabaseField(columnName = "green", canBeNull = false)
    private float green;

    public RGBCLog() {
    }

    @DatabaseField(columnName = "blue", canBeNull = false)
    private float blue;

    @DatabaseField(columnName = "clear", canBeNull = false)
    private float clear;

    @DatabaseField(columnName = "lux", canBeNull = false)
    private float lux;

    public float getRed() {
        return red;
    }

    public void setRed(float red) {
        this.red = red;
    }

    public float getGreen() {
        return green;
    }

    public void setGreen(float green) {
        this.green = green;
    }

    public float getBlue() {
        return blue;
    }

    public void setBlue(float blue) {
        this.blue = blue;
    }

    public float getClear() {
        return clear;
    }

    public void setClear(float clear) {
        this.clear = clear;
    }

    public float getLux() {
        return lux;
    }

    public void setLux(float lux) {
        this.lux = lux;
    }


    public RGBCLog(float red, float green, float blue, float clear, float lux) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.clear = clear;
        this.lux = lux;
    }
}
