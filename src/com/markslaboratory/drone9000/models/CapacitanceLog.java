package com.markslaboratory.drone9000.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "capacitance_log")
public class CapacitanceLog extends BaseLog  {

    @DatabaseField(columnName = "capacitance", canBeNull = false)
    private float capacitance;

    public CapacitanceLog() {
    }

    public float getCapacitance() {
        return capacitance;
    }

    public void setCapacitance(float capacitance) {
        this.capacitance = capacitance;
    }

    public CapacitanceLog(float capacitance) {
        this.capacitance = capacitance;
    }
}
