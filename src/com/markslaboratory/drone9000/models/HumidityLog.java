package com.markslaboratory.drone9000.models;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "humidity_log")
public class HumidityLog extends BaseLog {

    @DatabaseField(columnName = "humidity", canBeNull = false)
    private float humidity;

    public HumidityLog() {
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public HumidityLog(float humidity) {
        this.humidity = humidity;
    }
}
