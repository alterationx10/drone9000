package com.markslaboratory.drone9000.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "battery_log")
public class BatteryLog extends BaseLog {

    @DatabaseField(columnName = "voltage", canBeNull = false)
    private float voltage;

    public BatteryLog() {
    }

    public float getVoltage() {
        return voltage;
    }

    public void setVoltage(float voltage) {
        this.voltage = voltage;
    }

    public BatteryLog(float voltage) {
        this.voltage = voltage;
    }
}
