package com.markslaboratory.drone9000.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "pressure_log")
public class PressureLog extends BaseLog{

    @DatabaseField(columnName = "pressure", canBeNull = false)
    private float pressure;

    public PressureLog() {
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public PressureLog(float pressure) {
        this.pressure = pressure;
    }
}
