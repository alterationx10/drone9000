package com.markslaboratory.drone9000.models;


import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "temperature_log")
public class TemperatureLog extends BaseLog {


    @DatabaseField(columnName = "temperature", canBeNull = false)
    private float temperature;

    public TemperatureLog() {
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
    }

    public TemperatureLog(float temperature) {
        this.temperature = temperature;
    }
}
