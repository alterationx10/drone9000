package com.markslaboratory.drone9000.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "altitude_log")
public class AltitudeLog extends BaseLog {

    @DatabaseField(columnName = "altitude", canBeNull = false)
    private float altitude;

    public AltitudeLog() {
    }

    public float getAltitude() {
        return altitude;
    }

    public void setAltitude(float altitude) {
        this.altitude = altitude;
    }

    public AltitudeLog(float altitude) {
        this.altitude = altitude;
    }
}
