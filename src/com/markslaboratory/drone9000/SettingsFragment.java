package com.markslaboratory.drone9000;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.logging.LoggerScheduler;
import com.markslaboratory.drone9000.services.SensordroneService;
import com.markslaboratory.drone9000.tools.TXTReader;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.android.tools.DroneConnectionHelper;

public class SettingsFragment extends ServiceFragment implements DroneEventHandler{

    // Sensordrone select
    RelativeLayout sdLayout;
    TextView sdName;

    // Temperature
    RelativeLayout tempLayout;
    TextView tempUnitDisplay;

    // Pressure
    RelativeLayout presureLayout;
    TextView pressureUnitDisplay;

    // Logging
    RelativeLayout enableLogging;
    RelativeLayout disableLogging;
    LoggerScheduler scheduler;

    // Auto Connect
    RelativeLayout autoLayout;
    TextView autoStatus;

    // Back Key Override
    RelativeLayout backOverrideLayout;
    TextView overrideStatus;

    // Info Dialogs
    RelativeLayout generalUsage;
    RelativeLayout d9kEyeInfo;
    RelativeLayout whatsNewInfo;

    TXTReader infoReader;

    Dialog dialog;
    DroneConnectionHelper droneHelper;

    String[] thingsToLog = {
            "ADC",
            "Altitude",
            "Battery Voltage",
            "Capacitance",
            "CO",
            "Humidity",
            "IR Temperature",
            "Oxidizing Gas",
            "Pressure",
            "Reducing Gas",
            "R/G/B/C",
            "Temperature"
    };

    String[] logKeys = {
            PreferenceConstants.ADC_LOGGING,
            PreferenceConstants.ALTITUDE_LOGGING,
            PreferenceConstants.BATTERY_LOGGING,
            PreferenceConstants.CAPACITANCE_LOGGING,
            PreferenceConstants.CO_LOGGING,
            PreferenceConstants.HUMIDITY_LOGGING,
            PreferenceConstants.IR_LOGGING,
            PreferenceConstants.OX_LOGGING,
            PreferenceConstants.PRESSURE_LOGGING,
            PreferenceConstants.RED_LOGGING,
            PreferenceConstants.RGBC_LOGGING,
            PreferenceConstants.TEMPERATURE_LOGGING
    };

    public void setThingsToLogStatus() {
        this.thingsToLogStatus = new boolean[] {
            myPreferences.getBoolean(PreferenceConstants.ADC_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.ALTITUDE_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.BATTERY_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.CAPACITANCE_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.CO_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.HUMIDITY_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.IR_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.OX_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.PRESSURE_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.RED_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.RGBC_LOGGING, false),
                myPreferences.getBoolean(PreferenceConstants.TEMPERATURE_LOGGING, false)
        };
    }

    public boolean isEverythingOff() {
        setThingsToLogStatus();
        boolean status = true;
        for (int i = 0; i < thingsToLogStatus.length; i++) {
            status &= !thingsToLogStatus[i];
        }
        return status;
    }

    boolean[] thingsToLogStatus;

    public SettingsFragment(SensordroneService service) {
        super(service);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.settings_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setLEDColors(R.color.menu_purple);

        droneHelper = new DroneConnectionHelper();

        scheduler = new LoggerScheduler();

        infoReader = new TXTReader(getActivity());

        // Sensordrone
        sdLayout = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_sensordrone);
        sdName = (TextView)getActivity().findViewById(R.id.settings_tv_mac);
        sdLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // Disconnect if we are already connected
                if (droneService.sensorDrone.isConnected) {
                    droneService.sensorDrone.disconnect();
                }
                // Once to clear it, again to re-pair
                String MAC = myPreferences.getString(PreferenceConstants.SENSORDRONE, "");
                if (!MAC.equals("")) {
                    prefEditor.remove(PreferenceConstants.SENSORDRONE);
                    prefEditor.commit();
                    updateDisplay();
                } else {
//                    droneHelper.scanToConnect(droneService.sensorDrone, getActivity(), getActivity(), false);
                    droneHelper.connectFromPairedDevices(droneService.sensorDrone, getActivity());
                }
                return true;
            }
        });


        // Temperature
        tempLayout = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_temperature);
        tempUnitDisplay = (TextView)getActivity().findViewById(R.id.settings_tv_temp_units);
        tempLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                chooseUnitsDialog("Temperature Units", PreferenceConstants.TEMPERATURE_DISPLAY, PreferenceConstants.TEMPERATURE);
                return true;
            }
        });

        // Pressure
        presureLayout = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_pressure);
        pressureUnitDisplay = (TextView)getActivity().findViewById(R.id.settings_tv_pressure_units);
        presureLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                chooseUnitsDialog("Pressure Units", PreferenceConstants.PRESSURE_DISPLAY, PreferenceConstants.PRESSURE);
                return true;
            }
        });

        // Logging (Enable)
        enableLogging = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_logging_enable);
        enableLogging.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                enableLoggingDialog("Select to Enable");
                return true;
            }
        });

        // Logging (Disable)
        disableLogging = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_logging_disable);
        disableLogging.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                disableLoggingDialog("Select to Disable");
                return true;
            }
        });

        // Auto Connect
        autoLayout = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_autoconnect);
        autoStatus = (TextView)getActivity().findViewById(R.id.settings_tv_autoconnect);
        autoLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (myPreferences.getBoolean(PreferenceConstants.AUTO_CONNECT, false)) {
                    prefEditor.putBoolean(PreferenceConstants.AUTO_CONNECT, false).commit();
                } else {
                    prefEditor.putBoolean(PreferenceConstants.AUTO_CONNECT, true).commit();
                }
                updateDisplay();
                return true;
            }
        });

        // Back button override
        backOverrideLayout = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_back_override);
        overrideStatus = (TextView)getActivity().findViewById(R.id.settings_tv_overide_status);
        backOverrideLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (myPreferences.getBoolean(PreferenceConstants.BACK_BUTTON_OVERRIDE, false)) {
                    prefEditor.putBoolean(PreferenceConstants.BACK_BUTTON_OVERRIDE, false).commit();
                }
                else {
                    prefEditor.putBoolean(PreferenceConstants.BACK_BUTTON_OVERRIDE, true).commit();
                    // Inform the user what is going on
                    dialog = new Dialog(getActivity());
                    AlertDialog.Builder dBuilder = new AlertDialog.Builder(getActivity());
                    dBuilder.setTitle("Back Key Overridden");
                    dBuilder.setIcon(R.drawable.ic_launcher);
                    dBuilder.setMessage("You will now have to long-press the back key to exit.");
                    dBuilder.setPositiveButton("Affirmative", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //
                        }
                    });
                    dialog = dBuilder.create();
                    dialog.show();
                }
                updateDisplay();
                return true;
            }
        });


    }

    public void updateDisplay() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String MAC = myPreferences.getString(PreferenceConstants.SENSORDRONE, "No Sensordrone Stored!");
                sdName.setText(MAC);

                int TEMP = myPreferences.getInt(PreferenceConstants.TEMPERATURE, PreferenceConstants.CELSIUS);
                tempUnitDisplay.setText(PreferenceConstants.TEMPERATURE_DISPLAY[TEMP]);

                int PRESSURE = myPreferences.getInt(PreferenceConstants.PRESSURE, PreferenceConstants.MMHG);
                pressureUnitDisplay.setText(PreferenceConstants.PRESSURE_DISPLAY[PRESSURE]);

                if (myPreferences.getBoolean(PreferenceConstants.AUTO_CONNECT, false)) {
                    autoStatus.setText("(Enabled)");
                } else {
                    autoStatus.setText("(Disabled)");
                }

                if (myPreferences.getBoolean(PreferenceConstants.BACK_BUTTON_OVERRIDE, false)) {
                    overrideStatus.setText("(Enabled)");
                }
                else {
                    overrideStatus.setText("(Disabled)");
                }
            }
        });

        generalUsage = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_first_install);
        generalUsage.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                infoReader.displayTxtAlert("I am the DRONE9000 app", R.raw.first_install_tips);
                return true;
            }
        });

        d9kEyeInfo = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_eye_info);
        d9kEyeInfo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                infoReader.displayTxtAlert("Connections", R.raw.d9k_info);
                return true;
            }
        });

        whatsNewInfo = (RelativeLayout)getActivity().findViewById(R.id.settings_rl_whats_new);
        whatsNewInfo.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                infoReader.displayTxtAlert("What's New", R.raw.v1_0_3_whats_new);
                return true;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        droneService.sensorDrone.registerDroneListener(this);
        updateDisplay();
    }

    @Override
    public void onPause() {
        super.onPause();
        droneService.sensorDrone.unregisterDroneListener(this);
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {

        // If connection fires here, save the Sensordrone
        if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTED)) {
            prefEditor.putString(PreferenceConstants.SENSORDRONE, droneService.sensorDrone.lastMAC);
            prefEditor.commit();
            droneService.sensorDrone.setLEDs(RED_HI, GREEN_HI, BLUE_HI);
            updateDisplay();
            // We'll remain connected
        }

    }

    // Settings Dialogs
    public void chooseUnitsDialog(String TITLE, String[] UNITS, final String PREF_KEY) {

        Context context = getActivity();
        dialog = new Dialog(context);
        AlertDialog.Builder dBuilder = new AlertDialog.Builder(context);
        dBuilder.setTitle(TITLE);
        ListView unitList = new ListView(context);
        final ArrayAdapter<String> unitAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1,
                UNITS);
        unitList.setAdapter(unitAdapter);

        unitList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                prefEditor.putInt(PREF_KEY, position);
                prefEditor.commit();
                dialog.dismiss();
            }
        });


        dBuilder.setView(unitList);
        dialog = dBuilder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                updateDisplay();
            }
        });
        dialog.show();


    }

    // Logging (Enable) Dialogs
    public void enableLoggingDialog(String TITLE) {

        Context context = getActivity();
        dialog = new Dialog(context);
        AlertDialog.Builder dBuilder = new AlertDialog.Builder(context);
        dBuilder.setTitle(TITLE);
        final ListView sensorList = new ListView(context);
        final ArrayAdapter<String> sensorAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1);
        sensorList.setAdapter(sensorAdapter);

        // Only add things that are not enabled
        setThingsToLogStatus();
        for (int i = 0; i < thingsToLog.length; i ++) {
            if (!thingsToLogStatus[i]) {
                sensorAdapter.add(thingsToLog[i]);
            }
        }

        sensorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // What did we choose?
                String choice = sensorList.getItemAtPosition(position).toString();
                // Store the choice
                for (int i = 0; i < thingsToLog.length; i++) {
                    if (thingsToLog[i].equals(choice)) {
                        prefEditor.putBoolean(logKeys[i], true);
                        prefEditor.commit();
                    }
                }
                // Remove the item
                sensorAdapter.remove(choice);
            }
        });

        dBuilder.setPositiveButton("Restart Interval Immediately", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!isEverythingOff()) {
                    scheduler.setAlarm(getActivity());
                }
                dialog.dismiss();
            }
        });

        dBuilder.setNegativeButton("Continue at Current Interval", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dBuilder.setView(sensorList);
        dialog = dBuilder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                updateDisplay();
            }
        });
        dialog.show();
    }

    // Logging (Disable) Dialogs
    public void disableLoggingDialog(String TITLE) {

        Context context = getActivity();
        dialog = new Dialog(context);
        AlertDialog.Builder dBuilder = new AlertDialog.Builder(context);
        dBuilder.setTitle(TITLE);
        final ListView sensorList = new ListView(context);
        final ArrayAdapter<String> sensorAdapter = new ArrayAdapter<String>(
                context,
                android.R.layout.simple_list_item_1);
        sensorList.setAdapter(sensorAdapter);

        // Only add things that are  enabled
        setThingsToLogStatus();
        for (int i = 0; i < thingsToLog.length; i ++) {
            if (thingsToLogStatus[i]) {
                sensorAdapter.add(thingsToLog[i]);
            }
        }

        sensorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // What did we choose?
                String choice = sensorList.getItemAtPosition(position).toString();
                // Store the choice
                for (int i = 0; i < thingsToLog.length; i++) {
                    if (thingsToLog[i].equals(choice)) {
                        prefEditor.putBoolean(logKeys[i], false);
                        prefEditor.commit();
                    }
                }
                // Remove the item
                sensorAdapter.remove(choice);
            }
        });

        dBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dBuilder.setView(sensorList);
        dialog = dBuilder.create();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                updateDisplay();
                if (isEverythingOff()) {
                    scheduler.CancelAlarm(getActivity());
                }
            }
        });
        dialog.show();
    }



}
