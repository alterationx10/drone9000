package com.markslaboratory.drone9000.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.markslaboratory.drone9000.models.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper{

    private static final String DATABASE_NAME = "DRONE9000.db";
    private static final int DATABASE_VERSION = 1;


    // DAOs for ALL of our objects

    private Dao<ADCLog, Integer> adcDao = null;
    private RuntimeExceptionDao<ADCLog, Integer> adcRuntimeDao = null;

    private Dao<AltitudeLog, Integer> altitudeDao = null;
    private RuntimeExceptionDao<AltitudeLog, Integer> altitudeRuntimeDao = null;

    private  Dao<BatteryLog, Integer> batteryDao = null;
    private  RuntimeExceptionDao<BatteryLog, Integer> batteryRuntimeDao = null;

    private Dao<CapacitanceLog, Integer> capacitacneDao = null;
    private RuntimeExceptionDao<CapacitanceLog, Integer> capacitanceRuntimeDao = null;

    private Dao<COLog, Integer> coDao = null;
    private RuntimeExceptionDao<COLog, Integer> coRuntimeDao = null;

    private Dao<HumidityLog, Integer> humidityDao = null;
    private RuntimeExceptionDao<HumidityLog, Integer> humidityRuntimeDao = null;

    private Dao<IRLog, Integer> irDao = null;
    private RuntimeExceptionDao<IRLog, Integer> irRuntimeDao = null;

    private Dao<OxLog, Integer> oxDao = null;
    private RuntimeExceptionDao<OxLog, Integer> oxRuntimeDao = null;

    private Dao<PressureLog, Integer> pressureDao = null;
    private RuntimeExceptionDao<PressureLog, Integer> pressureRuntimeDao = null;

    private Dao<RedLog, Integer> redDao = null;
    private RuntimeExceptionDao<RedLog, Integer> redRuntimeDao = null;

    private Dao<RGBCLog, Integer> rgbcDao = null;
    private RuntimeExceptionDao<RGBCLog, Integer> rgbcRuntimeDao = null;

    private Dao<TemperatureLog, Integer> temperatureDao = null;
    private RuntimeExceptionDao<TemperatureLog, Integer> temperatureRuntimeDao = null;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {

        // Set up all of our tables
        try {
            TableUtils.createTable(connectionSource, ADCLog.class);
            TableUtils.createTable(connectionSource, AltitudeLog.class);
            TableUtils.createTable(connectionSource, BatteryLog.class);
            TableUtils.createTable(connectionSource, CapacitanceLog.class);
            TableUtils.createTable(connectionSource, COLog.class);
            TableUtils.createTable(connectionSource, HumidityLog.class);
            TableUtils.createTable(connectionSource, IRLog.class);
            TableUtils.createTable(connectionSource, OxLog.class);
            TableUtils.createTable(connectionSource, PressureLog.class);
            TableUtils.createTable(connectionSource, RedLog.class);
            TableUtils.createTable(connectionSource, RGBCLog.class);
            TableUtils.createTable(connectionSource, TemperatureLog.class);

//            TemperatureLog log = new TemperatureLog();

        } catch (SQLException e) {
            //
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i2) {

        try {
            TableUtils.dropTable(connectionSource, ADCLog.class, true);
            TableUtils.dropTable(connectionSource, AltitudeLog.class, true);
            TableUtils.dropTable(connectionSource, BatteryLog.class, true);
            TableUtils.dropTable(connectionSource, CapacitanceLog.class, true);
            TableUtils.dropTable(connectionSource, COLog.class, true);
            TableUtils.dropTable(connectionSource, HumidityLog.class, true);
            TableUtils.dropTable(connectionSource, IRLog.class, true);
            TableUtils.dropTable(connectionSource, OxLog.class, true);
            TableUtils.dropTable(connectionSource, PressureLog.class, true);
            TableUtils.dropTable(connectionSource, RedLog.class, true);
            TableUtils.dropTable(connectionSource, RGBCLog.class, true);
            TableUtils.dropTable(connectionSource, TemperatureLog.class, true);

            onCreate(sqLiteDatabase, connectionSource);
        }
        catch (SQLException e) {

        }
    }

    // Woofers and Tweeters


    public Dao<ADCLog, Integer> getAdcDao() throws SQLException {
        if (adcDao == null) {
            adcDao = getDao(ADCLog.class);
        }
        return adcDao;
    }

    public RuntimeExceptionDao<ADCLog, Integer> getAdcRuntimeDao() {
        if (adcRuntimeDao == null) {
            adcRuntimeDao = getRuntimeExceptionDao(ADCLog.class);
        }
        return adcRuntimeDao;
    }

    public Dao<AltitudeLog, Integer> getAltitudeDao() throws SQLException {
        if (altitudeDao == null) {
            altitudeDao = getDao(AltitudeLog.class);
        }
        return altitudeDao;
    }

    public RuntimeExceptionDao<AltitudeLog, Integer> getAltitudeRuntimeDao() {
        if (altitudeRuntimeDao == null) {
            altitudeRuntimeDao = getRuntimeExceptionDao(AltitudeLog.class);
        }
        return altitudeRuntimeDao;
    }

    public Dao<BatteryLog, Integer> getBatteryDao() throws SQLException {
        if (batteryDao == null) {
            batteryDao = getDao(BatteryLog.class);
        }
        return batteryDao;
    }

    public RuntimeExceptionDao<BatteryLog, Integer> getBatteryRuntimeDao() {
        if (batteryRuntimeDao == null) {
            batteryRuntimeDao = getRuntimeExceptionDao(BatteryLog.class);
        }
        return batteryRuntimeDao;
    }

    public Dao<CapacitanceLog, Integer> getCapacitacneDao() throws SQLException {
        if (capacitacneDao == null) {
            capacitacneDao = getDao(CapacitanceLog.class);
        }
        return capacitacneDao;
    }

    public RuntimeExceptionDao<CapacitanceLog, Integer> getCapacitanceRuntimeDao() {
        if (capacitanceRuntimeDao == null) {
            capacitanceRuntimeDao = getRuntimeExceptionDao(CapacitanceLog.class);
        }
        return capacitanceRuntimeDao;
    }

    public Dao<COLog, Integer> getCoDao() throws SQLException {
        if (coDao == null) {
            coDao = getDao(COLog.class);
        }
        return coDao;
    }

    public RuntimeExceptionDao<COLog, Integer> getCoRuntimeDao() {
        if (coRuntimeDao == null) {
            coRuntimeDao = getRuntimeExceptionDao(COLog.class);
        }
        return coRuntimeDao;
    }

    public Dao<HumidityLog, Integer> getHumidityDao() throws SQLException {
        if (humidityDao == null) {
            humidityDao = getDao(HumidityLog.class);
        }
        return humidityDao;
    }

    public RuntimeExceptionDao<HumidityLog, Integer> getHumidityRuntimeDao() {
        if (humidityRuntimeDao == null) {
            humidityRuntimeDao = getRuntimeExceptionDao(HumidityLog.class);
        }
        return humidityRuntimeDao;
    }

    public Dao<IRLog, Integer> getIrDao() throws SQLException {
        if (irDao == null) {
            irDao = getDao(IRLog.class);
        }
        return irDao;
    }

    public RuntimeExceptionDao<IRLog, Integer> getIrRuntimeDao() {
        if (irRuntimeDao == null) {
            irRuntimeDao = getRuntimeExceptionDao(IRLog.class);
        }
        return irRuntimeDao;
    }

    public Dao<OxLog, Integer> getOxDao() throws SQLException {
        if (oxDao == null) {
            oxDao = getDao(OxLog.class);
        }
        return oxDao;
    }

    public RuntimeExceptionDao<OxLog, Integer> getOxRuntimeDao() {
        if (oxRuntimeDao == null) {
            oxRuntimeDao = getRuntimeExceptionDao(OxLog.class);
        }
        return oxRuntimeDao;
    }

    public Dao<PressureLog, Integer> getPressureDao() throws SQLException {
        if (pressureDao == null) {
            pressureDao = getDao(PressureLog.class);
        }
        return pressureDao;
    }

    public RuntimeExceptionDao<PressureLog, Integer> getPressureRuntimeDao() {
        if (pressureRuntimeDao == null) {
            pressureRuntimeDao = getRuntimeExceptionDao(PressureLog.class);
        }
        return pressureRuntimeDao;
    }

    public Dao<RedLog, Integer> getRedDao() throws SQLException {
        if (redDao == null) {
            redDao = getDao(RedLog.class);
        }
        return redDao;
    }

    public RuntimeExceptionDao<RedLog, Integer> getRedRuntimeDao() {
        if (redRuntimeDao == null) {
            redRuntimeDao = getRuntimeExceptionDao(RedLog.class);
        }
        return redRuntimeDao;
    }

    public Dao<RGBCLog, Integer> getRgbcDao() throws SQLException {
        if (rgbcDao == null) {
            rgbcDao = getDao(RGBCLog.class);
        }
        return rgbcDao;
    }

    public RuntimeExceptionDao<RGBCLog, Integer> getRgbcRuntimeDao() {
        if (rgbcRuntimeDao == null) {
            rgbcRuntimeDao = getRuntimeExceptionDao(RGBCLog.class);
        }
        return rgbcRuntimeDao;
    }

    public Dao<TemperatureLog, Integer> getTemperatureDao() throws SQLException {
        if (temperatureDao == null) {
            temperatureDao = getDao(TemperatureLog.class);
        }
        return temperatureDao;
    }

    public RuntimeExceptionDao<TemperatureLog, Integer> getTemperatureRuntimeDao() {
        if (temperatureRuntimeDao == null) {
            temperatureRuntimeDao = getRuntimeExceptionDao(TemperatureLog.class);
        }
        return temperatureRuntimeDao;
    }

    @Override
    public void close() {
        super.close();
        // set our runtime DAOs to null
        adcRuntimeDao = null;
        altitudeRuntimeDao = null;
        batteryRuntimeDao = null;
        capacitanceRuntimeDao = null;
        coRuntimeDao = null;
        humidityRuntimeDao = null;
        irRuntimeDao = null;
        oxRuntimeDao = null;
        pressureRuntimeDao = null;
        redRuntimeDao = null;
        rgbcRuntimeDao = null;
        temperatureRuntimeDao = null;
    }

    public void clearDatabaseLog(Class LogClass) throws SQLException {
        ConnectionSource source = this.getConnectionSource();
        // Drop the old table
        TableUtils.dropTable(source, LogClass, true);
        // Re-Create it
        TableUtils.createTable(source, LogClass);
    }

    private void writeBaseHeader(FileOutputStream out) throws IOException {
        byte[] comma = ",".getBytes();
        out.write("id".getBytes());
        out.write(comma);
        out.write("unix_time".getBytes());
        out.write(comma);
        out.write("year".getBytes());
        out.write(comma);
        out.write("month".getBytes());
        out.write(comma);
        out.write("day".getBytes());
        out.write(comma);
        out.write("hour".getBytes());
        out.write(comma);
        out.write("minute".getBytes());
        out.write(comma);
        out.write("second".getBytes());
        out.write(comma);
    }

    public File makeADCCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_ADC.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("adc (12 bit 0-3V)".getBytes());
            out.write(comma);
            out.write("voltage (volts)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<ADCLog, Integer> dao = getAdcRuntimeDao();
            List<ADCLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getAdc()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getVoltage()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeAltitudeCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_Altitude.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("altitude (meter)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<AltitudeLog, Integer> dao = getAltitudeRuntimeDao();
            List<AltitudeLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getAltitude()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeBatteryCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_Battery.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("voltage (volts)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<BatteryLog, Integer> dao = getBatteryRuntimeDao();
            List<BatteryLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getVoltage()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeCapacitanceCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_Capacitance.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("capacitance (fF)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<CapacitanceLog, Integer> dao = getCapacitanceRuntimeDao();
            List<CapacitanceLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getCapacitance()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeCOCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_CO.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("carbon monoxide (ppm)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<COLog, Integer> dao = getCoRuntimeDao();
            List<COLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getPpm()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeHumidityCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_Humidity.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("Relative  Humidity (%)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<HumidityLog, Integer> dao = getHumidityRuntimeDao();
            List<HumidityLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getHumidity()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeIRCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_IR_Temperature.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("IR temperature (Celsius)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<IRLog, Integer> dao = getIrRuntimeDao();
            List<IRLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getTemperature()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeOxCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_Oxidizing_Gas.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("resistance (Ohm)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<OxLog, Integer> dao = getOxRuntimeDao();
            List<OxLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getResistance()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makePressureCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_Pressure.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("pressure (Pa)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<PressureLog, Integer> dao = getPressureRuntimeDao();
            List<PressureLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getPressure()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }



    public File makeRedCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_Reducing_Gas.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("resistance (Ohm)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<RedLog, Integer> dao = getRedRuntimeDao();
            List<RedLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getResistance()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeRGBCCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_RGBC.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("red (adc)".getBytes());
            out.write(comma);
            out.write("green (adc)".getBytes());
            out.write(comma);
            out.write("blue (adc)".getBytes());
            out.write(comma);
            out.write("clear (adc)".getBytes());
            out.write(comma);
            out.write("illuminance (lux)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<RGBCLog, Integer> dao = getRgbcRuntimeDao();
            List<RGBCLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getRed()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getGreen()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getBlue()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getClear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getLux()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

    public File makeTemperatureCSV() throws IOException {
        File csv = null;
        File root = Environment.getExternalStorageDirectory();

        if (root.canWrite()) {
            File dir = new File (root.getAbsolutePath() + "/DRONE9000");
            dir.mkdirs();
            csv = new File(dir, "D9K_Temperature.csv");
            FileOutputStream out = new FileOutputStream(csv);
            byte[] comma = ",".getBytes();
            byte[] newLine = "\n".getBytes();

            // Write the base header
            writeBaseHeader(out);
            // Write the specific header
            out.write("temperature (Celsius)".getBytes());
            out.write(newLine);

            // Get all of our Data
            RuntimeExceptionDao<TemperatureLog, Integer> dao = getTemperatureRuntimeDao();
            List<TemperatureLog> data = dao.queryForAll();
            // Loop over it
            for (int i = 0; i < data.size(); i++) {
                // Base stuff
                out.write(String.valueOf(data.get(i).getId()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getUnixTime()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getYear()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMonth()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getDay()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getHour()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getMinute()).getBytes());
                out.write(comma);
                out.write(String.valueOf(data.get(i).getSecond()).getBytes());
                out.write(comma);
                // Unique stuff
                out.write(String.valueOf(data.get(i).getTemperature()).getBytes());
                out.write(newLine);
            }
            out.close();
        }

        return csv;
    }

}
