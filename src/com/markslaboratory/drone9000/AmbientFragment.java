package com.markslaboratory.drone9000;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.markslaboratory.drone9000.services.SensordroneService;
import com.markslaboratory.drone9000.textview.AmbientCard;
import com.markslaboratory.drone9000.textview.HumidityTextView;
import com.markslaboratory.drone9000.textview.PressureTextView;
import com.markslaboratory.drone9000.textview.TemperatureTextView;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;

public class AmbientFragment extends ServiceFragment {


    // Sensor TextViews
    TemperatureTextView temp;
    HumidityTextView humidity;
    PressureTextView pressure;
    AmbientCard card;

    // Streamer
    DroneStreamer streamer;

    public AmbientFragment(SensordroneService service) {
        super(service);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        streamer = new DroneStreamer(droneService.sensorDrone, 1000) {
            @Override
            public void repeatableTask() {
                droneService.sensorDrone.measureTemperature();
                droneService.sensorDrone.measureHumidity();
                droneService.sensorDrone.measurePressure();
                myDrone.customEventNotify();
            }
        };


    }

    // Set up our Layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ambient_fragment, container, false);
        return rootView;
    }

    // Set up our GUI
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        temp = (TemperatureTextView)getActivity().findViewById(R.id.ambient_ttv_temperature);
        temp.setMyDrone(droneService.sensorDrone);
        temp.setMyActivity(getActivity());

        humidity = (HumidityTextView)getActivity().findViewById(R.id.ambient_htv_humidity);
        humidity.setMyDrone(droneService.sensorDrone);
        humidity.setMyActivity(getActivity());

        pressure = (PressureTextView)getActivity().findViewById(R.id.ambient_ptv_pressure);
        pressure.setMyDrone(droneService.sensorDrone);
        pressure.setMyActivity(getActivity());

        card = (AmbientCard)getActivity().findViewById(R.id.ambient_card);
        card.setMyDrone(droneService.sensorDrone);
        card.setMyActivity(getActivity());

        setLEDColors(R.color.menu_blue);



    }




    // Start up things
    @Override
    public void onResume() {
        super.onResume();

        // Enable our Sensors
        droneService.sensorDrone.enableTemperature();
        droneService.sensorDrone.enableHumidity();
        droneService.sensorDrone.enablePressure();

        // Register listeners
        droneService.sensorDrone.registerDroneListener(temp);
        droneService.sensorDrone.registerDroneListener(humidity);
        droneService.sensorDrone.registerDroneListener(pressure);
        droneService.sensorDrone.registerDroneListener(card);

        // Start Requesting Data
        streamer.start();
    }

    // Pause things
    @Override
    public void onPause() {
        super.onPause();

        // Stop requesting data
        streamer.stop();

        // Disable out sensors
        droneService.sensorDrone.disableTemperature();
        droneService.sensorDrone.disableHumidity();
        droneService.sensorDrone.disablePressure();

        // Unregister listeners
        droneService.sensorDrone.unregisterDroneListener(temp);
        droneService.sensorDrone.unregisterDroneListener(humidity);
        droneService.sensorDrone.unregisterDroneListener(pressure);
        droneService.sensorDrone.unregisterDroneListener(card);
    }


    // Clean up
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
