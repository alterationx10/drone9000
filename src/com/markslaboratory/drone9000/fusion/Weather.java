package com.markslaboratory.drone9000.fusion;


public class Weather {

    public static enum TEMPERATURE_UNIT{
        CELSIUS,
        FAHRENHEIT
    }

    public static enum HEAT_INDEX_RATING {
        NO_RATING,
        CAUTION,
        EXTREME_CAUTION,
        DANGER,
        EXTREME_DANGER
    }

    static float fahrenheitToCelsius(float temperature) {
        return (float) ((temperature - 32.0) * 5.0 / 9.0);
    }

    public static float celsiusToFahrenheit(float temperature) {
        return (float) (temperature * 9.0 / 5.0 + 32.0);
    }

    public static float heatIndex(float temperature, float humidity, TEMPERATURE_UNIT unit) {
        double HI = -1;
        double c1;
        double c2;
        double c3;
        double c4;
        double c5;
        double c6;
        double c7;
        double c8;
        double c9;

        // Equation is for Fahrenheit
        if (unit == TEMPERATURE_UNIT.CELSIUS) {
            temperature = celsiusToFahrenheit(temperature);
        }

        if (temperature >= 70) {
            c1 = 0.363445176;
            c2 = 0.988622465;
            c3 = 4.777114035;
            c4 = -0.114037667;
            c5 = -0.000850208;
            c6 = -0.020716198;
            c7 = 0.000687678;
            c8 = 0.000274954;
            c9 = 0;
        } else {
            return (float) HI;
        }

        HI = c1 +
                c2 * temperature +
                c3 * humidity +
                c4 * temperature * humidity +
                c5 * temperature * temperature +
                c6 * humidity * humidity +
                c7 * temperature * temperature * humidity +
                c8 * temperature * humidity * humidity +
                c9 * temperature * temperature * humidity * humidity;

        // Return Celsius if that's what was given
        if (unit == TEMPERATURE_UNIT.CELSIUS) {
            HI = fahrenheitToCelsius((float) HI);
        }

        return (float)HI;
    }

    static HEAT_INDEX_RATING heatIndexRating(float heatIndex, TEMPERATURE_UNIT unit) {
        // Equation is for Fahrenheit
        if (unit == TEMPERATURE_UNIT.CELSIUS) {
            heatIndex = celsiusToFahrenheit(heatIndex);
        }
        if (heatIndex >= 80 && heatIndex <= 90) {
            return  HEAT_INDEX_RATING.CAUTION;
        }
        else if (heatIndex > 90 && heatIndex <= 105) {
            return HEAT_INDEX_RATING.EXTREME_CAUTION;
        }
        else if (heatIndex > 105 && heatIndex <= 130) {
            return HEAT_INDEX_RATING.DANGER;
        } else if (heatIndex > 130) {
            return HEAT_INDEX_RATING.EXTREME_DANGER;
        } else {
            return HEAT_INDEX_RATING.NO_RATING;
        }
    }

}
