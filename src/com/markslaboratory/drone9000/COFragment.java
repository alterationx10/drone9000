package com.markslaboratory.drone9000;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.markslaboratory.drone9000.services.SensordroneService;
import com.markslaboratory.drone9000.textview.COTextView;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;

public class COFragment extends ServiceFragment {

    // Sensor TextViews
    COTextView ppm;

    // Streamer
    DroneStreamer streamer;

    public COFragment(SensordroneService service) {
        super(service);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        streamer = new DroneStreamer(droneService.sensorDrone, 750) {
            @Override
            public void repeatableTask() {
                droneService.sensorDrone.measurePrecisionGas();
            }
        };


    }

    // Set up our Layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.co_fragment, container, false);
        return rootView;
    }

    // Set up our GUI
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ppm = (COTextView)getActivity().findViewById(R.id.co_cotv_ppm);
        ppm.setMyDrone(droneService.sensorDrone);
        ppm.setMyActivity(getActivity());


        setLEDColors(R.color.menu_green);



    }




    // Start up things
    @Override
    public void onResume() {
        super.onResume();

        // Enable our Sensors
        droneService.sensorDrone.enablePrecisionGas();

        // Register listeners
        droneService.sensorDrone.registerDroneListener(ppm);

        // Start Requesting Data
        streamer.start();
    }

    // Pause things
    @Override
    public void onPause() {
        super.onPause();

        // Stop requesting data
        streamer.stop();

        // Disable out sensors
        droneService.sensorDrone.disablePrecisionGas();

        // Unregister listeners
        droneService.sensorDrone.unregisterDroneListener(ppm);
    }


    // Clean up
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
