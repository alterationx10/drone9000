package com.markslaboratory.drone9000.services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.*;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.util.Log;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.database.DatabaseHelper;
import com.markslaboratory.drone9000.logging.LoggerRetry;
import com.markslaboratory.drone9000.logging.LoggerScheduler;
import com.markslaboratory.drone9000.models.*;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.android.Drone;
import com.sensorcon.sensordrone.android.tools.DroneConnectionHelper;


/**
 * This service binds to SensordroneService and performs a Logging measurement
 */
public class LoggingService extends Service implements DroneEventHandler{


    DatabaseHelper db;

    private Drone sensorDrone;
    private String MAC;

    SharedPreferences myPreferences;
    SharedPreferences.Editor myEditor;
    PowerManager pm;
    PowerManager.WakeLock wl;

    private LoggerScheduler scheduler;
    private LoggerRetry retry;

    final int WAKELOCK_TIMEOUT = 15000;
    final int CONNECTION_TIMEOUT = 10000;
    final int MAX_RETRIES = 3;

    // THESE ARE NO LONGER NECESSARY, but I'll leave them in for now.
    // Booleans to prevent excessive triggering should the app already be open, ETC...
    private boolean logAdc = false;
    private boolean logAltitude = false;
    private boolean logBattery = false;
    private boolean logCapacitance = false;
    private boolean logCO = false;
    private boolean logHumidity = false;
    private boolean logIR = false;
    private boolean logOX = false;
    private boolean logPressure = false;
    private boolean logRed = false;
    private boolean logRGBC = false;
    private boolean logTemperature = false;

    private Handler myHandler;

    private boolean fromService;
    private int nRetries;

    @Override
    public IBinder onBind(Intent intent) {
        return null;

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("D9K", "LoggingService created...");
        myPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        myEditor = myPreferences.edit();
        myEditor.putBoolean(PreferenceConstants.LOGGING_SERVICE_STATUS, true).commit();
        pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        db = new DatabaseHelper(getApplicationContext());
        myHandler = new Handler();
        fromService = false;
        scheduler = new LoggerScheduler();
        retry = new LoggerRetry();
        sensorDrone = new Drone();
        sensorDrone.registerDroneListener(this);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // No point in continuing if Bluetooth isn't on
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!btAdapter.isEnabled()) {
            return Service.START_NOT_STICKY;
        }

        wl.acquire(WAKELOCK_TIMEOUT);

        // How many times have we run so far?
        nRetries = myPreferences.getInt(PreferenceConstants.LOGGING_ATTEMPTS, -1);
        // Initialize if necessary
        if (nRetries == -1) {
            myEditor.putInt(PreferenceConstants.LOGGING_ATTEMPTS, 0);
            nRetries = 0;
        }

        fromService = true;

        // Turn off future requests if no Sensordrone is stored.
        MAC = myPreferences.getString(PreferenceConstants.SENSORDRONE, "");
        if (MAC.equals("")) {
            scheduler.CancelAlarm(getApplicationContext());
            if (wl.isHeld()) {
                wl.release();
            }
            return Service.START_NOT_STICKY;
        }

        if (sensorDrone.btConnect(MAC)) {
            // If we connected, automatically disconnect
            myHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    sensorDrone.setLEDs(0, 0, 0);
                    sensorDrone.disconnect();
                }
            }, CONNECTION_TIMEOUT);
            // If we connected, then we don't need any pending retries
            retry.CancelAlarm(getApplicationContext());
            // Every successful connect resets the counter
            myEditor.putInt(PreferenceConstants.LOGGING_ATTEMPTS, 0);
        }
        else {
            // Schedule a re-try if we haven't reached the max
            if (nRetries < MAX_RETRIES) {
                retry.tryAgain(getApplicationContext());
            }
            // If we didn't connect, uptick our nRetries
            nRetries++;
        }

        return Service.START_NOT_STICKY;
    }




    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTED) && fromService) {
            fromService = false;
            sensorDrone.setLEDs(0, 0, 126);
            sensorDrone.customStatusNotifty();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.CUSTOM_STATUS)) {

            if (myPreferences.getBoolean(PreferenceConstants.ADC_LOGGING, false)) {
                logAdc = true;
                sensorDrone.enableADC();
            }
            if (myPreferences.getBoolean(PreferenceConstants.ALTITUDE_LOGGING, false)) {
                logAltitude = true;
                sensorDrone.enableAltitude();
            }
            if (myPreferences.getBoolean(PreferenceConstants.BATTERY_LOGGING, false)) {
                logBattery = true;
                sensorDrone.measureBatteryVoltage();
            }
            if (myPreferences.getBoolean(PreferenceConstants.CAPACITANCE_LOGGING, false)) {
                logCapacitance = true;
                sensorDrone.enableCapacitance();
            }
            if (myPreferences.getBoolean(PreferenceConstants.CO_LOGGING, false)) {
                logCO = true;
                sensorDrone.enablePrecisionGas();
            }
            if (myPreferences.getBoolean(PreferenceConstants.HUMIDITY_LOGGING, false)) {
                logHumidity = true;
                sensorDrone.enableHumidity();
            }
            if (myPreferences.getBoolean(PreferenceConstants.IR_LOGGING, false)) {
                logIR = true;
                sensorDrone.enableIRTemperature();
            }
            if (myPreferences.getBoolean(PreferenceConstants.OX_LOGGING, false)) {
                logOX = true;
                sensorDrone.enableOxidizingGas();
            }
            if (myPreferences.getBoolean(PreferenceConstants.PRESSURE_LOGGING, false)) {
                logPressure = true;
                sensorDrone.enablePressure();
            }
            if (myPreferences.getBoolean(PreferenceConstants.RED_LOGGING, false)) {
                logRed = true;
                sensorDrone.enableReducingGas();
            }
            if (myPreferences.getBoolean(PreferenceConstants.RGBC_LOGGING, false)) {
                logRGBC = true;
                sensorDrone.enableRGBC();
            }
            if (myPreferences.getBoolean(PreferenceConstants.TEMPERATURE_LOGGING, false)) {
                logTemperature = true;
                sensorDrone.enableTemperature();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.ADC_ENABLED) && logAdc) {
            sensorDrone.measureExternalADC();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.ADC_MEASURED) && logAdc) {
            logAdc = false;
            RuntimeExceptionDao<ADCLog, Integer> adcDao = db.getAdcRuntimeDao();
            ADCLog adc = new ADCLog(sensorDrone.externalADC, sensorDrone.externalADC_Volts);
            adcDao.create(adc);
            if (true) { //TODO replace the types later with variable that checks if it was previously enabled
                sensorDrone.disableADC();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.ALTITUDE_ENABLED) && logAltitude) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                //
            }
            sensorDrone.measureAltitude();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.ALTITUDE_MEASURED) && logAltitude) {
            logAltitude = false;
            RuntimeExceptionDao<AltitudeLog, Integer> altDao = db.getAltitudeRuntimeDao();
            AltitudeLog alt = new AltitudeLog(sensorDrone.altitude_Meters);
            altDao.create(alt);
            if (true) { //TODO
                sensorDrone.disableAltitude();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.BATTERY_VOLTAGE_MEASURED) && logBattery) {
            logBattery = false;
            RuntimeExceptionDao<BatteryLog, Integer> batDao = db.getBatteryRuntimeDao();
            BatteryLog bat = new BatteryLog(sensorDrone.batteryVoltage_Volts);
            batDao.create(bat);
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.CAPACITANCE_ENABLED) && logCapacitance) {
            sensorDrone.measureCapacitance();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.CAPCACITANCE_MEASURED) && logCapacitance) {
            logCapacitance = false;
            RuntimeExceptionDao<CapacitanceLog, Integer> capDao = db.getCapacitanceRuntimeDao();
            CapacitanceLog cap = new CapacitanceLog(sensorDrone.capacitance_femtoFarad);
            capDao.create(cap);
            if (true) { //TODO
                sensorDrone.disableCapacitance();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.PRECISION_GAS_ENABLED) && logCO) {
            sensorDrone.measurePrecisionGas();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.PRECISION_GAS_MEASURED) && logCO) {
            Log.d("D9K", "CO logged...");
            logCO = false;
            RuntimeExceptionDao<COLog, Integer> CODao = db.getCoRuntimeDao();
            COLog co = new COLog(sensorDrone.precisionGas_ppmCarbonMonoxide);
            CODao.create(co);
            if (true) { //TODO
                sensorDrone.disablePrecisionGas();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.HUMIDITY_ENABLED) && logHumidity) {
            sensorDrone.measureHumidity();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.HUMIDITY_MEASURED) && logHumidity) {
            logHumidity = false;
            RuntimeExceptionDao<HumidityLog, Integer> hDao = db.getHumidityRuntimeDao();
            HumidityLog hLog = new HumidityLog(sensorDrone.humidity_Percent);
            hDao.create(hLog);
            if (true) { //TODO
                sensorDrone.disableHumidity();
            }

        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.IR_TEMPERATURE_ENABLED) && logIR) {
            sensorDrone.measureIRTemperature();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.IR_TEMPERATURE_MEASURED) && logIR) {
            logIR = false;
            RuntimeExceptionDao<IRLog, Integer> irDao = db.getIrRuntimeDao();
            IRLog ir = new IRLog(sensorDrone.irTemperature_Celsius);
            irDao.create(ir);
            if (true) { //TODO
                sensorDrone.disableIRTemperature();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.OXIDIZING_GAS_ENABLED) && logOX) {
            sensorDrone.measureOxidizingGas();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.OXIDIZING_GAS_MEASURED) && logOX) {
            logOX = false;
            RuntimeExceptionDao<OxLog, Integer> oxDao = db.getOxRuntimeDao();
            OxLog ox = new OxLog(sensorDrone.oxidizingGas_Ohm);
            oxDao.create(ox);
            if (true) { //TODO
                sensorDrone.disableOxidizingGas();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.PRESSURE_ENABLED) && logPressure) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                //
            }
            sensorDrone.measurePressure();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.PRESSURE_MEASURED) && logPressure) {
            logPressure = false;
            RuntimeExceptionDao<PressureLog, Integer> pDao = db.getPressureRuntimeDao();
            PressureLog pLog = new PressureLog(sensorDrone.pressure_Pascals);
            pDao.create(pLog);
            if (true) { //TODO
                sensorDrone.disablePressure();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.REDUCING_GAS_ENABLED) && logRed) {
            sensorDrone.measureReducingGas();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.REDUCING_GAS_MEASURED) && logRed) {
            logRed = false;
            RuntimeExceptionDao<RedLog, Integer> redDao = db.getRedRuntimeDao();
            RedLog rLog = new RedLog(sensorDrone.reducingGas_Ohm);
            redDao.create(rLog);
            if (true) { //TODO
                sensorDrone.disableReducingGas();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.RGBC_ENABLED) && logRGBC) {
            sensorDrone.measureRGBC();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.RGBC_MEASURED) && logRGBC) {
            logRGBC = false;
            RuntimeExceptionDao<RGBCLog, Integer> rgbcDao = db.getRgbcRuntimeDao();
            RGBCLog rgbcLog = new RGBCLog(
                    sensorDrone.rgbcRedChannel,
                    sensorDrone.rgbcGreenChannel,
                    sensorDrone.rgbcBlueChannel,
                    sensorDrone.rgbcClearChannel,
                    sensorDrone.rgbcLux
            );
            rgbcDao.create(rgbcLog);
            if (true) { //TODO
                sensorDrone.disableRGBC();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.TEMPERATURE_ENABLED) && logTemperature) {
            sensorDrone.measureTemperature();
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.TEMPERATURE_MEASURED) && logTemperature) {
            logTemperature = false; // No repeats logged
            RuntimeExceptionDao<TemperatureLog, Integer> tempDao = db.getTemperatureRuntimeDao();
            TemperatureLog tempLog = new TemperatureLog(sensorDrone.temperature_Celsius);
            tempDao.create(tempLog);
            if (true) { //TODO
                sensorDrone.disableTemperature();
            }
        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.DISCONNECTED) ||
                droneEventObject.matches(DroneEventObject.droneEventType.CONNECTION_LOST)) {
            // We don't need our wake lock anymore
            if (wl.isHeld()) {
                wl.release();
            }
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sensorDrone.unregisterDroneListener(this);
        db.close();
        myEditor.putBoolean(PreferenceConstants.LOGGING_SERVICE_STATUS, false).commit();
    }
}
