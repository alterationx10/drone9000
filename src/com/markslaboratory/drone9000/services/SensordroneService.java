package com.markslaboratory.drone9000.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.sensorcon.sensordrone.android.Drone;

public class SensordroneService extends Service {

    final String TAG = "SensordroneService";

    public Drone sensorDrone;
    // Binder given to clients
    private final IBinder mBinder = new LocalBinder();


    public class LocalBinder extends Binder {
        public SensordroneService getService() {
            // Return this instance so clients can access the drone
            return SensordroneService.this;
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "Service started...");
        sensorDrone = new Drone();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(sensorDrone.isConnected) {
            sensorDrone.disconnectNow();
        }
    }
}
