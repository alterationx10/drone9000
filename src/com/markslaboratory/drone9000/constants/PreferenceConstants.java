package com.markslaboratory.drone9000.constants;


public class PreferenceConstants {

    // Preference TAGS
    public static final String SENSORDRONE = "SENSORDRONE";
    public static final String TEMPERATURE = "TEMPERATURE";
    public static final String PRESSURE = "PRESSURE";
    public static final String ALTITUDE = "ALTITUDE";
    public static final String AUTO_CONNECT = "AUTO_CONNECT";
    // Logging
    public static final String LOGGING_SERVICE_STATUS = "LOGGING_SERVICE_STATUS";
    public static final String TEMPERATURE_LOGGING = "TEMPERATURE_LOGGING";
    public static final String HUMIDITY_LOGGING = "HUMIDITY_LOGGING";
    public static final String PRESSURE_LOGGING = "PRESSURE_LOGGING";
    public static final String ADC_LOGGING = "ADC_LOGGING";
    public static final String ALTITUDE_LOGGING = "ALTITUDE_LOGGING";
    public static final String BATTERY_LOGGING = "BATTERY_LOGGING";
    public static final String CAPACITANCE_LOGGING = "CAPACITANCE_LOGGING";
    public static final String CO_LOGGING = "CO_LOGGING";
    public static final String IR_LOGGING = "IR_LOGGING";
    public static final String OX_LOGGING = "OX_LOGGING";
    public static final String RED_LOGGING = "RED_LOGGING";
    public static final String RGBC_LOGGING = "RGBC_LOGGING";
    public static final String LOGGING_ATTEMPTS = "LOGGING_ATTEMPTS";

    public static final String GRAPH_X_DATA = "GRAPH_X_DATA";
    public static final String GRAPH_Y_DATA = "GRAPH_Y_DATA";
    public static final String GRAPH_TITLE = "GRAPH_TITLE";
    public static final String GRAPH_X_LABEL = "GRAPH_X_LABEL";
    public static final String GRAPH_Y_LABEL = "GRAPH_Y_LABEL";
    public static final String GRAPH_SERIES_TITLE = "GRAPH_SERIES_TITLE";

    public static final String BACK_BUTTON_OVERRIDE = "BACK_BUTTON_OVERRIDE";

    public static final String FIRST_BOOT = "FIRST_BOOT";

    public static final String LAST_KNOWN_VERSION_CODE = "LAST_KNOWN_VERSION_CODE";


    // Preference keys
    public static final int MMHG = 0;
    public static final int HPA = 1;
    public static final int ATM = 2;

    public static final int CELSIUS = 0;
    public static final int FAHRENHEIT = 1;

    public static final int METER = 0;
    public static final int FEET = 1;

    // Preference display strings
    public static final String[] PRESSURE_DISPLAY = {
            " mmHg",
            " hPa",
            " Atm"
    };

    public static final String[] TEMPERATURE_DISPLAY = {
            " \u00B0C",
            " \u00B0F"
    };

    public static final String[] ALTITUDE_DISPLAY = {
            " m",
            " ft"
    };



}
