package com.markslaboratory.drone9000.logging;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.markslaboratory.drone9000.services.LoggingService;

public class LoggerRetry extends BroadcastReceiver {


    // Timing
    final int RETRY = 15;


    public LoggerRetry() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent myIntent = new Intent(context, LoggingService.class);
        context.startService(myIntent);
    }

    public void tryAgain(Context context) {
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, this.getClass());
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        int minutes = RETRY * 60 * 1000; // Minutes * Seconds * milliseconds
        am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + minutes, pi);
    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, this.getClass());
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }




}
