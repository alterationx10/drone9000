package com.markslaboratory.drone9000.logging;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.markslaboratory.drone9000.services.LoggingService;

public class LoggerScheduler extends BroadcastReceiver {


    // Timing
    final int INTERVAL = 60; // 1 Hour
    final int RETRY = 5; // 5 Minutes


    public LoggerScheduler() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent myIntent = new Intent(context, LoggingService.class);
        context.startService(myIntent);
    }


    public void setAlarm(Context context) {
        AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, this.getClass());
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        int minutes = INTERVAL * 60 * 1000; // Minutes * Seconds * milliseconds
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), minutes, pi);

    }

    public void CancelAlarm(Context context) {
        Intent intent = new Intent(context, this.getClass());
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }



}
