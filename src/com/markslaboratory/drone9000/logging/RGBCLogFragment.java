package com.markslaboratory.drone9000.logging;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.markslaboratory.drone9000.database.DatabaseHelper;
import com.markslaboratory.drone9000.models.ADCLog;
import com.markslaboratory.drone9000.models.RGBCLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.util.List;

public class RGBCLogFragment extends BaseLogFragment {



    RuntimeExceptionDao<RGBCLog, Integer> dao;
    List<RGBCLog> loggedData;


    public RGBCLogFragment(SensordroneService service) {
        super(service);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Logged R/G/B/C Data");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        logFetcher = new DataLogFetcher() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                db = new DatabaseHelper(getActivity());
                dao = db.getRgbcRuntimeDao();
            }

            @Override
            protected Void doInBackground(Void... params) {
                loggedData = dao.queryForAll();
                db.close();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                checkForEmptyList(loggedData.size());

                for (int i= loggedData.size()-1; i >= 0; i--) {
                    String display = "";
                    display += String.format(myLocale, "%.0f", loggedData.get(i).getRed()) + "/";
                    display += String.format(myLocale, "%.0f", loggedData.get(i).getGreen()) + "/";
                    display += String.format(myLocale, "%.0f", loggedData.get(i).getBlue()) + "/";
                    display += String.format(myLocale, "%.0f", loggedData.get(i).getClear()) + "\n";
                    display += String.format(myLocale, "%.0f", loggedData.get(i).getLux()) + " lux\n";
                    display += "(" + getFormattedTimestamp(loggedData.get(i)) + ")";
                    listAdapter.add(display);
                }
            }
        };

        logFetcher.execute();


    }

}
