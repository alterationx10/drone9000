package com.markslaboratory.drone9000.logging;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.markslaboratory.drone9000.models.ADCLog;
import com.markslaboratory.drone9000.models.BatteryLog;
import com.markslaboratory.drone9000.models.RGBCLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class RGBCLogManagerFragment extends BaseLogManagerFragment {


    public RGBCLogManagerFragment(SensordroneService service, Fragment logViewFragment) {
        super(service, logViewFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Logged RGBC Data");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void showGraph() {
        // Add different graphs per colr here later!
        RuntimeExceptionDao<RGBCLog, Integer> dao = db.getRgbcRuntimeDao();
        List<RGBCLog> log = dao.queryForAll();
        if (log.size() == 0) {
            hasNoData();
            return;
        }
        // Less than three points will crash the graph
        else if (log.size() < 3) {
            notnoughData();
            return;
        }

        // Initialize our arrays
        float[] xData = new float[log.size()];
        float[] yData = new float[log.size()];

        // Get our data and load it into the arrays
        for (int i = 0; i < log.size(); i++) {
            yData[i] = log.get(i).getLux();
            xData[i] = log.get(i).getUnixTime();
        }

        launchGraph(xData, yData, "Illuminance", "lux");
    }

    @Override
    public void showSend() {
        try {
            File log = db.makeRGBCCSV();
            sendFile(log);
        } catch (IOException e) {
            Toast.makeText(getActivity(),"Trouble generating csv file!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void confirmDelete() {
        try {
            db.clearDatabaseLog(RGBCLog.class);
        } catch (SQLException e) {
            clearDatabaseError();
        }
    }
}
