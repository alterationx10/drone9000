package com.markslaboratory.drone9000.logging;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.database.DatabaseHelper;
import com.markslaboratory.drone9000.models.PressureLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.util.List;

public class PressureLogFragment extends BaseLogFragment {



    RuntimeExceptionDao<PressureLog, Integer> dao;
    List<PressureLog> loggedData;

    int preferredUnit;
    String unit;

    public PressureLogFragment(SensordroneService service) {
        super(service);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Logged Pressure Data");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        logFetcher = new DataLogFetcher() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                db = new DatabaseHelper(getActivity());
                dao = db.getPressureRuntimeDao();
            }

            @Override
            protected Void doInBackground(Void... params) {
                loggedData = dao.queryForAll();
                db.close();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                preferredUnit = myPreferences.getInt(PreferenceConstants.PRESSURE, PreferenceConstants.HPA);
                unit = PreferenceConstants.PRESSURE_DISPLAY[preferredUnit];

                checkForEmptyList(loggedData.size());

                for (int i= loggedData.size()-1; i >= 0; i--) {

                    String display = "";

                    float pressure = loggedData.get(i).getPressure(); // Pascal
                    if (preferredUnit == PreferenceConstants.MMHG) {
                        pressure = (float) (pressure * 0.00750061683);
                        display += String.format(myLocale, "%.2f", pressure);
                    }
                    else if (preferredUnit == PreferenceConstants.ATM) {
                        pressure = (float) (pressure * 9.86923267e-6);
                        display += String.format(myLocale, "%.3f", pressure);
                    }
                    else {
                        pressure = pressure/100; // hPa is default
                        display += String.format(myLocale, "%.2f", pressure);
                    }

                    display += unit + "\n";
                    display += "(" + getFormattedTimestamp(loggedData.get(i)) + ")";
                    listAdapter.add(display);
                }
            }
        };

        logFetcher.execute();


    }

}
