package com.markslaboratory.drone9000.logging;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.database.DatabaseHelper;
import com.markslaboratory.drone9000.fusion.Weather;
import com.markslaboratory.drone9000.models.IRLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.util.List;

public class IRLogFragment extends BaseLogFragment {



    RuntimeExceptionDao<IRLog, Integer> dao;
    List<IRLog> loggedData;

    int preferredUnit;
    String unit;

    public IRLogFragment(SensordroneService service) {
        super(service);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Logged IR Temperature Data");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        logFetcher = new DataLogFetcher() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                db = new DatabaseHelper(getActivity());
                dao = db.getIrRuntimeDao();
            }

            @Override
            protected Void doInBackground(Void... params) {
                loggedData = dao.queryForAll();
                db.close();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                preferredUnit = myPreferences.getInt(PreferenceConstants.TEMPERATURE, PreferenceConstants.CELSIUS);
                unit = PreferenceConstants.TEMPERATURE_DISPLAY[preferredUnit];

                checkForEmptyList(loggedData.size());

                for (int i= loggedData.size()-1; i >= 0; i--) {

                    float temp = loggedData.get(i).getTemperature(); // Celsius
                    if (preferredUnit == PreferenceConstants.FAHRENHEIT) {
                        temp = Weather.celsiusToFahrenheit(temp);
                    }

                    String display = "";
                    display += String.format(myLocale, "%.2f", temp) + unit + "\n";
                    display += "(" + getFormattedTimestamp(loggedData.get(i)) + ")";
                    listAdapter.add(display);
                }
            }
        };

        logFetcher.execute();


    }

}
