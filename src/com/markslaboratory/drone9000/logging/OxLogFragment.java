package com.markslaboratory.drone9000.logging;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.markslaboratory.drone9000.database.DatabaseHelper;
import com.markslaboratory.drone9000.models.OxLog;
import com.markslaboratory.drone9000.models.RedLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.util.List;

public class OxLogFragment extends BaseLogFragment {



    RuntimeExceptionDao<OxLog, Integer> dao;
    List<OxLog> loggedData;


    public OxLogFragment(SensordroneService service) {
        super(service);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Logged Oxidizing Gas Data");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        logFetcher = new DataLogFetcher() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                db = new DatabaseHelper(getActivity());
                dao = db.getOxRuntimeDao();
            }

            @Override
            protected Void doInBackground(Void... params) {
                loggedData = dao.queryForAll();
                db.close();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                checkForEmptyList(loggedData.size());

                for (int i= loggedData.size()-1; i >= 0; i--) {
                    String display = "";
                    display += String.format(myLocale, "%.0f", loggedData.get(i).getResistance()) + " Ohm\n";
                    display += "(" + getFormattedTimestamp(loggedData.get(i)) + ")";
                    listAdapter.add(display);
                }
            }
        };

        logFetcher.execute();



    }

}
