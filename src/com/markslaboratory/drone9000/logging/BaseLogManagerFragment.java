package com.markslaboratory.drone9000.logging;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.markslaboratory.drone9000.GraphActivity;
import com.markslaboratory.drone9000.R;
import com.markslaboratory.drone9000.ServiceFragment;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.database.DatabaseHelper;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.io.File;
import java.util.Locale;

public abstract class BaseLogManagerFragment extends ServiceFragment{

    DatabaseHelper db;

    RelativeLayout viewData;
    RelativeLayout graphData;
    RelativeLayout sendData;
    RelativeLayout deleteData;

    Fragment logViewFragment;

    public BaseLogManagerFragment(SensordroneService service, Fragment logViewFragment) {
        super(service);
        this.logViewFragment = logViewFragment;
    }

    Locale myLocale;
    SharedPreferences myPreferences;


    public BaseLogManagerFragment(SensordroneService service) {
        super(service);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLEDColors(R.color.menu_orange);
        myLocale = Locale.getDefault();
        db = new DatabaseHelper(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.log_manager_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        myPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        viewData = (RelativeLayout)getActivity().findViewById(R.id.log_manager_view_data);
        viewData.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showData();
                return true;
            }
        });
        graphData = (RelativeLayout)getActivity().findViewById(R.id.log_manager_graph_data);
        graphData.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showGraph();
                return true;
            }
        });
        sendData = (RelativeLayout)getActivity().findViewById(R.id.log_manager_send_data);
        sendData.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showSend();
                return true;
            }
        });
        deleteData = (RelativeLayout)getActivity().findViewById(R.id.log_manager_delete_data);
        deleteData.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Dialog dialog = new Dialog(getActivity());
                AlertDialog.Builder dBuilder = new AlertDialog.Builder(getActivity());
                dBuilder.setTitle("I'm afraid I can't do that");
                dBuilder.setMessage("Are you sure you want to clear the data?\n\nThis is not reversible.");
                dBuilder.setIcon(R.drawable.ic_launcher);
                dBuilder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        confirmDelete();
                    }
                });
                dBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getActivity(), "Data preserved.", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                dialog = dBuilder.create();
                dialog.show();
                return true;
            }
        });
    }


    public void showData() {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, logViewFragment);
        // We want to add this one to the backstack
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();
    }

    public void clearDatabaseError() {
        Toast.makeText(getActivity(), "There was an error clearing the database", Toast.LENGTH_SHORT).show();
    }

    public void hasNoData() {
        Toast.makeText(getActivity(), "There is no data to graph.", Toast.LENGTH_SHORT).show();
    }

    public void notnoughData() {
        Toast.makeText(getActivity(), "You need at least 3 logged records to view the graph.", Toast.LENGTH_SHORT).show();
    }

    public abstract void showGraph();
    public abstract void showSend();
    public abstract void confirmDelete();



    public void launchGraph(float[] xData, float[] yData, String title, String yLabel) {
        Intent graphIntent = new Intent(getActivity(), GraphActivity.class);
        Bundle data = new Bundle();
        // Load it into the bundle
        data.putFloatArray(PreferenceConstants.GRAPH_X_DATA, xData);
        data.putFloatArray(PreferenceConstants.GRAPH_Y_DATA, yData);
        data.putString(PreferenceConstants.GRAPH_TITLE, title);
        data.putString(PreferenceConstants.GRAPH_Y_LABEL, yLabel);
        // Load the bundle
        graphIntent.putExtras(data);
        // Make it so
        startActivity(graphIntent);
    }

    public void sendFile(File dataLog) {
        Uri fileUri = Uri.fromFile(dataLog);
        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "DRONE900 Data");
        sendIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        sendIntent.setType("text/html");
        startActivity(sendIntent);
    }

}
