package com.markslaboratory.drone9000.logging;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.markslaboratory.drone9000.R;
import com.markslaboratory.drone9000.ServiceFragment;
import com.markslaboratory.drone9000.arrayadapter.RightMenuAdapter;
import com.markslaboratory.drone9000.database.DatabaseHelper;
import com.markslaboratory.drone9000.models.BaseLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.util.Locale;

public class BaseLogFragment extends ServiceFragment{

    DatabaseHelper db;
    ArrayAdapter<String> listAdapter;
    ListView dataList;
    Locale myLocale;
    SharedPreferences myPreferences;


    public BaseLogFragment(SensordroneService service) {
        super(service);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLEDColors(R.color.menu_orange);
        myLocale = Locale.getDefault();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listAdapter = new RightMenuAdapter(getActivity(), R.layout.logging_list_item);
        dataList = (ListView)getActivity().findViewById(R.id.logging_list);
        dataList.setAdapter(listAdapter);
        myPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.logging_fragment, container, false);
        return rootView;
    }

    public String getFormattedTimestamp(BaseLog coreLog) {
        String timeStamp = "";
        timeStamp += String.format("%02d", coreLog.getDay()) + "/";
        timeStamp += String.format("%02d", coreLog.getMonth() + 1) + "/";
        timeStamp += String.format("%04d", coreLog.getYear()) + " - ";
        timeStamp += String.format("%02d", coreLog.getHour()) + ":";
        timeStamp += String.format("%02d", coreLog.getMinute()) + ":";
        timeStamp += String.format("%02d", coreLog.getSecond());
        return timeStamp;
    }

    public void checkForEmptyList(int listSize) {
        if (listSize == 0) {
            String noData = "No logged data found";
            listAdapter.add(noData);
        }
        return;
    }
}
