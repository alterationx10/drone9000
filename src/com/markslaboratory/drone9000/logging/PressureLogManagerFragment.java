package com.markslaboratory.drone9000.logging;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.models.ADCLog;
import com.markslaboratory.drone9000.models.BatteryLog;
import com.markslaboratory.drone9000.models.PressureLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class PressureLogManagerFragment extends BaseLogManagerFragment {

    public PressureLogManagerFragment(SensordroneService service, Fragment logViewFragment) {
        super(service, logViewFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Logged Pressure Data");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void showGraph() {
        RuntimeExceptionDao<PressureLog, Integer> dao = db.getPressureRuntimeDao();
        List<PressureLog> log = dao.queryForAll();
        if (log.size() == 0) {
            hasNoData();
            return;
        }
        // Less than three points will crash the graph
        else if (log.size() < 3) {
            notnoughData();
            return;
        }

        int preferredUnit = myPreferences.getInt(PreferenceConstants.PRESSURE, PreferenceConstants.HPA);
        String unit = PreferenceConstants.PRESSURE_DISPLAY[preferredUnit];

        // Initialize our arrays
        float[] xData = new float[log.size()];
        float[] yData = new float[log.size()];

        // Get our data and load it into the arrays
        for (int i = 0; i < log.size(); i++) {
            yData[i] = log.get(i).getPressure();
            xData[i] = log.get(i).getUnixTime();
        }

        // Convert if necessary
        if (preferredUnit == PreferenceConstants.MMHG) {
            for (int i = 0; i < log.size(); i++) {
                yData[i] = (float) (yData[i] * 0.00750061683);
            }
        }
        else if (preferredUnit == PreferenceConstants.ATM) {
            for (int i = 0; i < log.size(); i++) {
                yData[i] = (float) (yData[i] * 9.86923267e-6);
            }
        }

        launchGraph(xData, yData, "Pressure", unit);
    }

    @Override
    public void showSend() {
        try {
            File log = db.makePressureCSV();
            sendFile(log);
        } catch (IOException e) {
            Toast.makeText(getActivity(),"Trouble generating csv file!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void confirmDelete() {
        try {
            db.clearDatabaseLog(PressureLog.class);
        } catch (SQLException e) {
            clearDatabaseError();
        }
    }
}
