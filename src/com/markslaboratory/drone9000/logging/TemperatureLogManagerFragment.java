package com.markslaboratory.drone9000.logging;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.fusion.Weather;
import com.markslaboratory.drone9000.models.TemperatureLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class TemperatureLogManagerFragment extends BaseLogManagerFragment {

    public TemperatureLogManagerFragment(SensordroneService service, Fragment logViewFragment) {
        super(service, logViewFragment);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("Logged Temperature Data");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void showGraph() {
        RuntimeExceptionDao<TemperatureLog, Integer> dao = db.getTemperatureRuntimeDao();
        List<TemperatureLog> log = dao.queryForAll();
        if (log.size() == 0) {
            hasNoData();
            return;
        }
        // Less than three points will crash the graph
        else if (log.size() < 3) {
            notnoughData();
            return;
        }

        // Units
        int preferredUnit = myPreferences.getInt(PreferenceConstants.TEMPERATURE, PreferenceConstants.CELSIUS);
        String unit = PreferenceConstants.TEMPERATURE_DISPLAY[preferredUnit];

        // Initialize our arrays
        float[] xData = new float[log.size()];
        float[] yData = new float[log.size()];

        // Get our data and load it into the arrays
        for (int i = 0; i < log.size(); i++) {
            yData[i] = log.get(i).getTemperature();
            xData[i] = log.get(i).getUnixTime();
        }

        // Convert if necessary
        if (preferredUnit == PreferenceConstants.FAHRENHEIT) {
            for (int i = 0; i < log.size(); i++) {
                yData[i] = Weather.celsiusToFahrenheit(yData[i]);
            }
        }

        launchGraph(xData, yData, "Temperature", unit);
    }

    @Override
    public void showSend() {
        try {
            File log = db.makeTemperatureCSV();
            sendFile(log);
        } catch (IOException e) {
            Toast.makeText(getActivity(),"Trouble generating csv file!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void confirmDelete() {
        try {
            db.clearDatabaseLog(TemperatureLog.class);
        } catch (SQLException e) {
            clearDatabaseError();
        }
    }
}
