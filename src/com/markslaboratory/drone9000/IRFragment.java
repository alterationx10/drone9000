package com.markslaboratory.drone9000;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.markslaboratory.drone9000.services.SensordroneService;
import com.markslaboratory.drone9000.textview.IRTextView;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;

public class IRFragment extends ServiceFragment {

    // Sensor TextViews
    IRTextView temp;

    // Streamer
    DroneStreamer streamer;

    public IRFragment(SensordroneService service) {
        super(service);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        streamer = new DroneStreamer(droneService.sensorDrone, 750) {
            @Override
            public void repeatableTask() {
                droneService.sensorDrone.measureIRTemperature();
            }
        };


    }

    // Set up our Layout
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.ir_fragment, container, false);
        return rootView;
    }

    // Set up our GUI
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        temp = (IRTextView)getActivity().findViewById(R.id.ir_irtv_temperature);
        temp.setMyDrone(droneService.sensorDrone);
        temp.setMyActivity(getActivity());


        setLEDColors(R.color.menu_red);



    }




    // Start up things
    @Override
    public void onResume() {
        super.onResume();
        droneService.sensorDrone.setLEDs(RED_HI, GREEN_HI, BLUE_HI);

        // Enable our Sensors
        droneService.sensorDrone.enableIRTemperature();

        // Register listeners
        droneService.sensorDrone.registerDroneListener(temp);

        // Start Requesting Data
        streamer.start();
    }

    // Pause things
    @Override
    public void onPause() {
        super.onPause();
        droneService.sensorDrone.setLEDs(RED_LOW, GREEN_LOW, BLUE_LOW);

        // Stop requesting data
        streamer.stop();

        // Disable out sensors
        droneService.sensorDrone.disableIRTemperature();

        // Unregister listeners
        droneService.sensorDrone.unregisterDroneListener(temp);
    }


    // Clean up
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
