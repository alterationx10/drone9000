package com.markslaboratory.drone9000;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

public class HomeFragment extends Fragment {

    TextView serviceStatus;
    ToggleButton serviceToggle;
    MainActivity masterActivity;


    public HomeFragment(MainActivity mainActivity) {
        this.masterActivity = mainActivity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        serviceStatus = (TextView)getActivity().findViewById(R.id.home_tv_status);

        serviceToggle = (ToggleButton)getActivity().findViewById(R.id.home_tb_service);
        serviceToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serviceToggle.isChecked()) {
                    masterActivity.droneService.sensorDrone.btConnect(masterActivity.droneService.sensorDrone.lastMAC);
                } else {
                    //
                }

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (masterActivity.droneService.sensorDrone.isConnected) {
            serviceToggle.setChecked(true);
        } else {
            serviceToggle.setChecked(false);
        }

    }
}
