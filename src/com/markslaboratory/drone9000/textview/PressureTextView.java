package com.markslaboratory.drone9000.textview;


import android.content.Context;
import android.util.AttributeSet;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.sensorcon.sensordrone.DroneEventObject;

public class PressureTextView extends FancyTextView {

    int UNIT;
    String DISPLAY_UNIT;

    public PressureTextView(Context context) {
        super(context);
        loadPrefrences();
    }

    public PressureTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadPrefrences();
    }

    public PressureTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        loadPrefrences();
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        // Don't do anything if our Activity isn't there!
        if (myActivity == null) {
            return;
        }

        if (droneEventObject.matches(DroneEventObject.droneEventType.TEMPERATURE_MEASURED)) {
            // Handle units here eventually...
            myActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float pressure;
                    if (UNIT == PreferenceConstants.ATM) {
                        pressure = myDrone.pressure_Atmospheres;
                    }
                    else if (UNIT == PreferenceConstants.HPA) {
                        pressure = myDrone.pressure_Pascals / 100;
                    }
                    else {
                        pressure = myDrone.pressure_Torr;
                    }
                    PressureTextView.this.setText(String.format("%.1f", pressure) + DISPLAY_UNIT);
                }
            });

        }
    }

    public void loadPrefrences() {
        UNIT = myPreferences.getInt(PreferenceConstants.PRESSURE, PreferenceConstants.MMHG);
        DISPLAY_UNIT = PreferenceConstants.PRESSURE_DISPLAY[UNIT];
    }
}
