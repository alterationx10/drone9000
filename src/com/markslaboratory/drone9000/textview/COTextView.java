package com.markslaboratory.drone9000.textview;


import android.content.Context;
import android.util.AttributeSet;
import com.sensorcon.sensordrone.DroneEventObject;

public class COTextView extends FancyTextView {

    public COTextView(Context context) {
        super(context);
    }

    public COTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public COTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (myActivity == null) {
            return;
        }

        if (droneEventObject.matches(DroneEventObject.droneEventType.PRECISION_GAS_MEASURED)) {
            // Handle units here eventually...
            myActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    COTextView.this.setText(String.format("%.1f",myDrone.precisionGas_ppmCarbonMonoxide) + " ppm");
                }
            });

        }    }
}
