package com.markslaboratory.drone9000.textview;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;

public class BatteryTextView extends FancyTextView implements DroneEventHandler{

    private Handler myHandler;

    public BatteryTextView(Context context) {
        super(context);
        myHandler = new Handler();
    }

    public BatteryTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        myHandler = new Handler();
    }

    public BatteryTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        myHandler = new Handler();
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (droneEventObject.matches(DroneEventObject.droneEventType.BATTERY_VOLTAGE_MEASURED)) {
            if (myActivity == null) {
                return;
            }

            myActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    BatteryTextView.this.setText("Battery: " + String.format("%.2f", myDrone.batteryVoltage_Volts) + " V");
                    // I'll keep updating myself every 5 seconds.
                    // If I ever lose connection, it wont fire, and the loop will be broken.
                    myHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            myDrone.measureBatteryVoltage();
                        }
                    }, 5000);
                }
            });
        }
    }
}
