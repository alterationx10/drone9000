package com.markslaboratory.drone9000.textview;


import android.content.Context;
import android.util.AttributeSet;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.sensorcon.sensordrone.DroneEventObject;

public class TemperatureTextView extends FancyTextView {

    int UNIT;
    String DISPLAY_UNIT;

    public TemperatureTextView(Context context) {
        super(context);
        loadPrefrences();
    }

    public TemperatureTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadPrefrences();
    }

    public TemperatureTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        loadPrefrences();
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        // Don't do anything if our Activity isn't there!
        if (myActivity == null) {
            return;
        }

        if (droneEventObject.matches(DroneEventObject.droneEventType.TEMPERATURE_MEASURED)) {
            myActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float temp;
                    if (UNIT == PreferenceConstants.FAHRENHEIT) {
                        temp = myDrone.temperature_Fahrenheit;
                    } else {
                        temp = myDrone.temperature_Celsius;
                    }
                    TemperatureTextView.this.setText(String.format("%.1f", temp) + DISPLAY_UNIT);
                }
            });

        }
    }

    public void loadPrefrences() {
        UNIT = myPreferences.getInt(PreferenceConstants.TEMPERATURE, PreferenceConstants.CELSIUS);
        DISPLAY_UNIT = PreferenceConstants.TEMPERATURE_DISPLAY[UNIT];
    }
}
