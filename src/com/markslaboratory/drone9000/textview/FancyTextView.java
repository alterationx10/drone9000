package com.markslaboratory.drone9000.textview;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.widget.TextView;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.android.Drone;

/**
 * A Fancy TextView that will update itself!
 */
abstract class FancyTextView extends TextView  implements DroneEventHandler {

    // Preferences
    SharedPreferences myPreferences;

    public void setMyDrone(Drone myDrone) {
        this.myDrone = myDrone;
    }

    public void setMyActivity(Activity myActivity) {
        this.myActivity = myActivity;
    }

    // Things we'll need
    public Drone myDrone;
    public Activity myActivity;

    // Get all of our default Constructors in
    public FancyTextView(Context context) {
        super(context);
        loadPreferenceManager(context);
    }

    public FancyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadPreferenceManager(context);
    }

    public FancyTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        loadPreferenceManager(context);
    }

    public void loadPreferenceManager(Context context) {
        myPreferences = PreferenceManager.getDefaultSharedPreferences(context);

    }

}
