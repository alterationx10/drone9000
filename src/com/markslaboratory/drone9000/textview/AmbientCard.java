package com.markslaboratory.drone9000.textview;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.markslaboratory.drone9000.MainActivity;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.fusion.Weather;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;

public class AmbientCard extends FancyTextView implements DroneEventHandler {

    private boolean visible = false;
    int UNIT;
    String DISPLAY_UNIT;

    public AmbientCard(Context context) {
        super(context);
        loadPrefrences();
    }

    public AmbientCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadPrefrences();
    }

    public AmbientCard(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        loadPrefrences();
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (this.myActivity == null) {
            return;
        }

        // We will use the CUSTOM_EVENT to fire

        if (droneEventObject.matches(DroneEventObject.droneEventType.CUSTOM_EVENT)) {


            // Assume we're going to be visible unless fagged otherwise
            this.visible = true;
            // Check if We should display info about heat index
            if (myDrone.temperature_Fahrenheit > 70) {
                final float feelsLike;

                if (UNIT == PreferenceConstants.FAHRENHEIT) {
                    feelsLike = Weather.heatIndex(myDrone.temperature_Fahrenheit, myDrone.humidity_Percent, Weather.TEMPERATURE_UNIT.FAHRENHEIT);
                }
                else {
                    feelsLike = Weather.heatIndex(myDrone.temperature_Celsius, myDrone.humidity_Percent, Weather.TEMPERATURE_UNIT.CELSIUS);
                }

                myActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AmbientCard.this.setText("Feels like " + String.format("%.0f", feelsLike) + DISPLAY_UNIT);
                    }
                });

            } else {
                // Too cold for Heat Index
                this.visible = false;
            }



            // This runs on the UI thread.
            myDrone.customStatusNotifty();

        }
        else if (droneEventObject.matches(DroneEventObject.droneEventType.CUSTOM_STATUS)) {
            myActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // Show/Hide our view based upon readings
                    if (AmbientCard.this.visible == true) {
                        AmbientCard.this.setVisibility(View.VISIBLE);
                    }
                    else {
                        AmbientCard.this.setVisibility(View.INVISIBLE);
                    }
                }
            });

        }

    }


    public void loadPrefrences() {
        UNIT = myPreferences.getInt(PreferenceConstants.TEMPERATURE, PreferenceConstants.CELSIUS);
        DISPLAY_UNIT = PreferenceConstants.TEMPERATURE_DISPLAY[UNIT];
    }

}
