package com.markslaboratory.drone9000.textview;


import android.content.Context;
import android.util.AttributeSet;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.sensorcon.sensordrone.DroneEventObject;

public class IRTextView extends FancyTextView {

    int UNIT;
    String DISPLAY_UNIT;

    public IRTextView(Context context) {
        super(context);
        loadPrefrences();
    }

    public IRTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        loadPrefrences();
    }

    public IRTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        loadPrefrences();
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (myActivity == null) {
            return;
        }

        if (droneEventObject.matches(DroneEventObject.droneEventType.IR_TEMPERATURE_MEASURED)) {
            // Handle units here eventually...
            myActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    float temp;
                    if (UNIT == PreferenceConstants.FAHRENHEIT) {
                        temp = myDrone.irTemperature_Fahrenheit;
                    } else {
                        temp = myDrone.irTemperature_Celsius;
                    }
                    IRTextView.this.setText(String.format("%.1f", temp) + DISPLAY_UNIT);
                }
            });

        }
    }

    public void loadPrefrences() {
        UNIT = myPreferences.getInt(PreferenceConstants.TEMPERATURE, PreferenceConstants.CELSIUS);
        DISPLAY_UNIT = PreferenceConstants.TEMPERATURE_DISPLAY[UNIT];
    }
}
