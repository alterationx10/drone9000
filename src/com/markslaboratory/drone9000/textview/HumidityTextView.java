package com.markslaboratory.drone9000.textview;


import android.content.Context;
import android.util.AttributeSet;
import com.sensorcon.sensordrone.DroneEventObject;

public class HumidityTextView extends FancyTextView {
    public HumidityTextView(Context context) {
        super(context);
    }

    public HumidityTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HumidityTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        // Don't do anything if our Activity isn't there!
        if (myActivity == null) {
            return;
        }

        if (droneEventObject.matches(DroneEventObject.droneEventType.TEMPERATURE_MEASURED)) {
            myActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    HumidityTextView.this.setText(String.format("%.1f",myDrone.humidity_Percent) + " %");
                }
            });

        }
    }
}
