package com.markslaboratory.drone9000;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import com.androidplot.Plot;
import com.androidplot.xy.BoundaryMode;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;
import com.markslaboratory.drone9000.constants.PreferenceConstants;

import java.text.*;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class GraphActivity extends Activity implements View.OnTouchListener {

    private XYPlot mySimpleXYPlot;
    private Button resetButton;
    private SimpleXYSeries series = null;
    private PointF minXY;
    private PointF maxXY;

    float[] xBundleData;
    float[] yBundleData;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph_layout);
        mySimpleXYPlot = (XYPlot) findViewById(R.id.mySimpleXYPlot);


        // Load our Bundled Data
        Intent dataInent = getIntent();
        if (dataInent != null) {
            xBundleData = dataInent.getFloatArrayExtra(PreferenceConstants.GRAPH_X_DATA);
            yBundleData = dataInent.getFloatArrayExtra(PreferenceConstants.GRAPH_Y_DATA);
            mySimpleXYPlot.setTitle(dataInent.getStringExtra(PreferenceConstants.GRAPH_TITLE));
            series = new SimpleXYSeries("");
            mySimpleXYPlot.setRangeLabel(dataInent.getStringExtra(PreferenceConstants.GRAPH_Y_LABEL));
            mySimpleXYPlot.setDomainLabel("Date (day-month-year hour:minute)");
        } else {
            this.finish();
        }


        resetButton = (Button) findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                minXY.x = series.getX(0).floatValue();
                maxXY.x = series.getX(series.size() - 1).floatValue();
                mySimpleXYPlot.setDomainBoundaries(minXY.x, maxXY.x, BoundaryMode.FIXED);

                // pre 0.5.1 users should use postRedraw() instead.
                mySimpleXYPlot.redraw();
            }
        });
        mySimpleXYPlot.setOnTouchListener(this);
        mySimpleXYPlot.getGraphWidget().setTicksPerRangeLabel(2);
        mySimpleXYPlot.getGraphWidget().setTicksPerDomainLabel(2);
        mySimpleXYPlot.getGraphWidget().getBackgroundPaint().setColor(Color.TRANSPARENT);



        // Adjust the left margin based on the number of digits to display
        float rangeMargin;
        float rangeMax = maxValue(yBundleData);

        if (rangeMax < 10) {
            mySimpleXYPlot.getGraphWidget().setRangeValueFormat(
                    new DecimalFormat("#######.##"));
        }
        else if (rangeMax < 1000) {
            mySimpleXYPlot.getGraphWidget().setRangeValueFormat(
                    new DecimalFormat("#######.#"));
        }
        else {
            mySimpleXYPlot.getGraphWidget().setRangeValueFormat(
                    new DecimalFormat("#######"));
        }

        if (rangeMax < 1e2) {
            rangeMargin = 25;
        }
        else if (rangeMax < 1e3) {
            rangeMargin = 35;
        }
        else if (rangeMax < 1e4) {
            rangeMargin = 45;
        }
        else if (rangeMax < 1e5) {
            rangeMargin = 55;
        }
        else {
            rangeMargin = 65;
        }
        // Adjust the margins
        mySimpleXYPlot.getGraphWidget().setPaddingLeft(rangeMargin); // THIS ONE FOR UNIT/NUMBER SPACING

//        mySimpleXYPlot.getGraphWidget().setMarginLeft(rangeMargin);
//        mySimpleXYPlot.setPlotMarginLeft(rangeMargin);
//        mySimpleXYPlot.setPlotPaddingLeft(rangeMargin);

        // We want TIME
        mySimpleXYPlot.getGraphWidget().setDomainValueFormat(new Format() {

            private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm");

            @Override
            public StringBuffer format(Object object, StringBuffer buffer, FieldPosition field) {
                long timestamp = ((Number) object).longValue();
                Date date = new Date(timestamp);
                return dateFormat.format(date, buffer, field);
            }

            @Override
            public Object parseObject(String string, ParsePosition position) {
                return null;
            }
        });


        mySimpleXYPlot.getGraphWidget().setRangeLabelWidth(25);


        mySimpleXYPlot.setBorderStyle(Plot.BorderStyle.NONE, null, null);
        //mySimpleXYPlot.disableAllMarkup();

        // Loa the data
        populateSeries(series, 5);

        int pointRed = Color.red(getResources().getColor(R.color.fragment_purple));
        int pointGreen = Color.green(getResources().getColor(R.color.fragment_purple));
        int pointBlue = Color.blue(getResources().getColor(R.color.fragment_purple));
        int fillRed = Color.red(getResources().getColor(R.color.menu_purple));
        int fillGreen = Color.green(getResources().getColor(R.color.menu_purple));
        int fillBlue = Color.blue(getResources().getColor(R.color.menu_purple));
        mySimpleXYPlot.addSeries(series,
                new LineAndPointFormatter(
                        Color.rgb(pointRed, pointGreen, pointBlue),
                        Color.rgb(pointRed, pointGreen, pointBlue),
                        Color.argb(175, fillRed, fillGreen, fillBlue),
                        null));

        mySimpleXYPlot.redraw();
        mySimpleXYPlot.calculateMinMaxVals();
        minXY = new PointF(mySimpleXYPlot.getCalculatedMinX().floatValue(),
                mySimpleXYPlot.getCalculatedMinY().floatValue());
        maxXY = new PointF(mySimpleXYPlot.getCalculatedMaxX().floatValue(),
                mySimpleXYPlot.getCalculatedMaxY().floatValue());
    }

    private void populateSeries(SimpleXYSeries series, int max) {
        for (int i = 0; i < xBundleData.length; i++) {
            series.addLast(xBundleData[i], yBundleData[i]);
        }
    }

    // Definition of the touch states
    static final int NONE = 0;
    static final int ONE_FINGER_DRAG = 1;
    static final int TWO_FINGERS_DRAG = 2;
    int mode = NONE;

    PointF firstFinger;
    float distBetweenFingers;
    boolean stopThread = false;

    @Override
    public boolean onTouch(View arg0, MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN: // Start gesture
                firstFinger = new PointF(event.getX(), event.getY());
                mode = ONE_FINGER_DRAG;
                stopThread = true;
                break;
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_POINTER_UP:
                mode = NONE;
                break;
            case MotionEvent.ACTION_POINTER_DOWN: // second finger
                distBetweenFingers = spacing(event);
                // the distance check is done to avoid false alarms
                if (distBetweenFingers > 5f) {
                    mode = TWO_FINGERS_DRAG;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (mode == ONE_FINGER_DRAG) {
                    PointF oldFirstFinger = firstFinger;
                    firstFinger = new PointF(event.getX(), event.getY());
                    scroll(oldFirstFinger.x - firstFinger.x);
                    mySimpleXYPlot.setDomainBoundaries(minXY.x, maxXY.x,
                            BoundaryMode.FIXED);
                    mySimpleXYPlot.redraw();

                } else if (mode == TWO_FINGERS_DRAG) {
                    float oldDist = distBetweenFingers;
                    distBetweenFingers = spacing(event);
                    zoom(oldDist / distBetweenFingers);
                    mySimpleXYPlot.setDomainBoundaries(minXY.x, maxXY.x,
                            BoundaryMode.FIXED);
                    mySimpleXYPlot.redraw();
                }
                break;
        }
        return true;
    }

    private void zoom(float scale) {
        float domainSpan = maxXY.x - minXY.x;
        float domainMidPoint = maxXY.x - domainSpan / 2.0f;
        float offset = domainSpan * scale / 2.0f;

        minXY.x = domainMidPoint - offset;
        maxXY.x = domainMidPoint + offset;

        minXY.x = Math.min(minXY.x, series.getX(series.size() - 3)
                .floatValue());
        maxXY.x = Math.max(maxXY.x, series.getX(1).floatValue());
        clampToDomainBounds(domainSpan);
    }

    private void scroll(float pan) {
        float domainSpan = maxXY.x - minXY.x;
        float step = domainSpan / mySimpleXYPlot.getWidth();
        float offset = pan * step;
        minXY.x = minXY.x + offset;
        maxXY.x = maxXY.x + offset;
        clampToDomainBounds(domainSpan);
    }

    private void clampToDomainBounds(float domainSpan) {
        float leftBoundary = series.getX(0).floatValue();
        float rightBoundary = series.getX(series.size() - 1).floatValue();
        // enforce left scroll boundary:
        if (minXY.x < leftBoundary) {
            minXY.x = leftBoundary;
            maxXY.x = leftBoundary + domainSpan;
        } else if (maxXY.x > series.getX(series.size() - 1).floatValue()) {
            maxXY.x = rightBoundary;
            minXY.x = rightBoundary - domainSpan;
        }
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return FloatMath.sqrt(x * x + y * y);
    }

    private float maxValue(float[] rangeData) {
        float max = rangeData[0]; // Start with something
        for (int i = 0; i < rangeData.length; i++) {
            if (rangeData[i] > max) {
                max = rangeData[i];
            }
        }
        return max;
    }
}
