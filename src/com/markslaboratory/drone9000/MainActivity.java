package com.markslaboratory.drone9000;

import android.app.*;
import android.bluetooth.BluetoothAdapter;
import android.content.*;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.markslaboratory.drone9000.arrayadapter.LeftMenuAdapter;
import com.markslaboratory.drone9000.arrayadapter.RightMenuAdapter;
import com.markslaboratory.drone9000.constants.PreferenceConstants;
import com.markslaboratory.drone9000.logging.*;
import com.markslaboratory.drone9000.services.SensordroneService;
import com.markslaboratory.drone9000.tools.TXTReader;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.android.tools.DroneConnectionHelper;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;

public class MainActivity extends Activity {

    private String TAG = "MainActivity";
    private String drawerLabels[];
    private DrawerLayout myDrawer;
    private ListView drawerList;
    private LinearLayout statLayout;
//    public BatteryTextView btv;
    private CharSequence actionBarTitle;

    public SensordroneService droneService;

    private DroneStreamer myStreamer;

    public boolean mBound;

    private boolean exitFromMenu = false;
    private boolean showLowBattery = true;

    private ListView rightList;
    private ArrayAdapter<String> rightAdapter;
    private SharedPreferences myPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Anything");
        setContentView(R.layout.main);

        myPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());


        // Start our service. It will auto-connect to the Sensordrone
        bindToService();

        drawerLabels = new String[]{
                "Ambient Conditions",
                "Carbon Monoxide",
                "IR Thermometer",
                "Sensordrone Status",
//                "Logged Data",
                "Settings",
                "Exit"
        };

        myDrawer = (DrawerLayout)findViewById(R.id.drawer_layout);
        drawerList = (ListView)findViewById(R.id.left_drawer);

        drawerList.setAdapter(new LeftMenuAdapter(this, R.layout.drawer_list_item, drawerLabels));
        drawerList.setOnItemClickListener(new DrawerItemClickListener());

//        statLayout = (LinearLayout)findViewById(R.id.right_drawer);


//        btv = (BatteryTextView)getLayoutInflater().inflate(R.layout.battery_status, null);

//        btv.setText("Starting...");
//        btv.setMyActivity(this);

//        statLayout.addView(btv);


        // Check for Bluetooth
        DroneConnectionHelper btChecker = new DroneConnectionHelper();
        btChecker.isBTEnabled(this,  BluetoothAdapter.getDefaultAdapter());


        getActionBar().setDisplayHomeAsUpEnabled(true);

        final String[] logItems = {
                "ADC",
                "Altitude",
                "Battery",
                "Capacitance",
                "CO",
                "Humidity",
                "IR Temperature",
                "Oxidizing Gas",
                "Pressure",
                "Reducing Gas",
                "R/G/B/C",
                "Temperature"
        };

        rightList = (ListView)findViewById(R.id.right_drawer);
        rightAdapter = new RightMenuAdapter(this, R.layout.drawer_list_item, logItems);
        rightList.setAdapter(rightAdapter);
        rightList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Fragment fragment = null;
                if (rightList.getItemAtPosition(position).equals(logItems[0])) {
//                    fragment = new ADCLogFragment(droneService);
                    fragment = new ADCLogManagerFragment(droneService,  new ADCLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[1])) {
//                    fragment = new AltitudeLogFragment(droneService);
                    fragment = new AltitudeLogManagerFragment(droneService, new AltitudeLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[2])) {
//                    fragment = new BatteryLogFragment(droneService);
                    fragment = new BatteryLogManagerFragment(droneService, new BatteryLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[3])) {
//                    fragment = new CapacitanceLogFragment(droneService);
                    fragment = new CapacitanceLogManagerFragment(droneService, new CapacitanceLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[4])) {
//                    fragment = new COLogFragment(droneService);
                    fragment = new COLogManagerFragment(droneService, new COLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[5])) {
//                    fragment = new HumidityLogFragment(droneService);
                      fragment = new HumidityLogManagerFragment(droneService, new HumidityLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[6])) {
//                    fragment = new IRLogFragment(droneService);
                    fragment = new IRLogManagerFragment(droneService, new IRLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[7])) {
//                    fragment = new OxLogFragment(droneService);
                    fragment = new OxLogManagerFragment(droneService, new OxLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[8])) {
//                    fragment = new PressureLogFragment(droneService);
                    fragment = new PressureLogManagerFragment(droneService, new PressureLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[9])) {
//                    fragment = new RedLogFragment(droneService);
                    fragment = new RedLogManagerFragment(droneService, new RedLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[10])) {
//                    fragment = new RGBCLogFragment(droneService);
                    fragment = new RGBCLogManagerFragment(droneService, new RGBCLogFragment(droneService));
                }
                else if (rightList.getItemAtPosition(position).equals(logItems[11])) {
//                    fragment = new TemperatureLogFragment(droneService);
                    fragment = new TemperatureLogManagerFragment(droneService, new TemperatureLogFragment(droneService));
                }

                // Load the fragment
                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    // Clear any old things. Don't know of any other way to clear the back stack...
                    fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, fragment);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    fragmentTransaction.commit();
                }

                rightList.setItemChecked(position, false); // Uncheck the item
                myDrawer.closeDrawer(rightList);
            }
        });

//        firstLaunchDisplay(this);
        whatsNewDialog(new TXTReader(MainActivity.this).readRawTXT(R.raw.v1_0_3_whats_new));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Take us to our greeting fragment
                Fragment fragment = new GreetingsFragment(droneService);
                FragmentManager fragmentManager = getFragmentManager();
                // Clear any old things. Don't know of any other way to clear the back stack...
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.content_frame, fragment);
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                fragmentTransaction.commit();

                // Close the Drawers if they are open
                if (myDrawer.isDrawerOpen(rightList)){
                    myDrawer.closeDrawer(rightList);
                }
                if (myDrawer.isDrawerOpen(drawerList)) {
                    myDrawer.closeDrawer(drawerList);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    /** Swaps fragments in the main content view */
    private void selectItem(int position) {
        // Don't do anything if the service isn't bound!
        if (!mBound) {
            return;
        }

        //Set the appropriate fragment to load
        Fragment fragment;

        if (position == 0) {
            fragment = new AmbientFragment(droneService);

        }
        else if (position == 1) {
            fragment = new COFragment(droneService);
        }
        else if (position == 2) {
            fragment = new IRFragment(droneService);
        }
        else if (position == 3) {
//            fragment = new LoggingFragment(droneService);
            fragment = new StatusFragment((droneService));
        }
        else if (position == 4) {
            fragment = new SettingsFragment(droneService);
        }
        else if (position == 5) {
            exitFromMenu = true;
            // This is the exit command, typically triggered by DISCONNECTED
            // What if we aren't connected?
            if (droneService.sensorDrone.isConnected) {
                droneService.sensorDrone.setLEDs(0,0,0);
                droneService.sensorDrone.disconnect();
            } else {
                drawerList.setItemChecked(position, true);
                myDrawer.closeDrawer(drawerList);
                MainActivity.this.finish();
            }

            drawerList.setItemChecked(position, true);
            myDrawer.closeDrawer(drawerList);
            return;
        }
        else {
            // Default Fragment
            fragment = new AmbientFragment(droneService);
        }
        fragment.setRetainInstance(true);


        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getFragmentManager();
        // Clear any old things. Don't know of any other way to clear the back stack...
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.commit();

        // Highlight the selected item, update the title, and close the drawer
        drawerList.setItemChecked(position, true);
        setTitle(drawerLabels[position]);
        myDrawer.closeDrawer(drawerList);
    }

    // We need to reset the app if there is ever a disconnect
    private DroneEventHandler disconnectHandler = new DroneEventHandler() {
        @Override
        public void parseEvent(DroneEventObject droneEventObject) {
            if (droneEventObject.matches(DroneEventObject.droneEventType.CONNECTION_LOST)) {

                // Take us to our greeting fragment
                Fragment fragment = new GreetingsFragment(droneService);
                fragment.setRetainInstance(true);
                FragmentManager fragmentManager = getFragmentManager();
                // Clear any old things. Don't know of any other way to clear the back stack...
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();

            }
            else if (droneEventObject.matches(DroneEventObject.droneEventType.DISCONNECTED) && exitFromMenu) {
                MainActivity.this.finish();
            }
            else if (droneEventObject.matches(DroneEventObject.droneEventType.LOW_BATTERY) && showLowBattery) {
                // Only show once
                showLowBattery = false;
                lowBatteryWarn();
            }
        }
    };

    @Override
    public void setTitle(CharSequence title) {
        actionBarTitle = title;
        getActionBar().setTitle(actionBarTitle);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (droneService.sensorDrone.isConnected) {
            droneService.sensorDrone.setLEDs(0, 0, 0);
            droneService.sensorDrone.disconnect();
        }
        unbindService(mConnection);
        droneService.sensorDrone.unregisterDroneListener(disconnectHandler);
//        droneService.sensorDrone.unregisterDroneListener(btv);
    }

    /** Defines callbacks for service binding, passed to bindService() */
    public ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            SensordroneService.LocalBinder binder = (SensordroneService.LocalBinder) service;
            droneService = binder.getService();
            mBound = true;

            // Start to handle disconnects
            droneService.sensorDrone.registerDroneListener(disconnectHandler);



            // Set up our fancy text view
//            btv.setMyDrone(droneService.sensorDrone);
//            droneService.sensorDrone.registerDroneListener(btv);


            // Take us to our greeting fragment
            Fragment fragment = new GreetingsFragment(droneService);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame,fragment).commit();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;

            // Take our bat and go home
            MainActivity.this.finish();
        }
    };

    public void bindToService() {
        Intent myIntent = new Intent(getApplicationContext(), SensordroneService.class);
        bindService(myIntent, mConnection, Context.BIND_AUTO_CREATE);
    }


    // Settings Dialogs
    public void lowBatteryWarn() {

        Context context = MainActivity.this;
        Dialog dialog;
        AlertDialog.Builder dBuilder = new AlertDialog.Builder(context);
        dBuilder.setTitle("Low Battery!");
        dBuilder.setMessage("I know I've used some energy recently, but I can give you my complete assurance that my work will be back to normal if you charge me up.");
        dBuilder.setIcon(R.drawable.ic_launcher);
        dBuilder.setPositiveButton("Affirmative", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog = dBuilder.create();
        dialog.show();


    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        Log.d("D9K", String.valueOf(keyCode));
//        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
//            event.startTracking();
//            return true; // Back key shouldnt respond!
//        }
//        return super.onKeyDown(keyCode, event);
//    }


    @Override
    public void onBackPressed() {

        // Should we override the back button?
        if (!myPreferences.getBoolean(PreferenceConstants.BACK_BUTTON_OVERRIDE, false)) {
            super.onBackPressed();
            return;
        }

        // If we made it this far, then we want to override the back button.
        // We still need to return normally if there is something on the backstack though.
        FragmentManager fragmentManager = getFragmentManager();
        int backStackCount = fragmentManager.getBackStackEntryCount();

        if (backStackCount > 0) {
            // Handle our back outs
            super.onBackPressed();
        } else {
            return;
        }
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myPreferences.getBoolean(PreferenceConstants.BACK_BUTTON_OVERRIDE, false)) {
            this.finish();
        }
        return super.onKeyLongPress(keyCode, event);
    }

//    public void firstLaunchDisplay(Context context) {
//
//        if(!myPreferences.getBoolean(PreferenceConstants.FIRST_BOOT, true)) {
//            return;
//        }
//        Dialog dialog;
//        AlertDialog.Builder builder = new AlertDialog.Builder(context);
//        builder.setIcon(R.drawable.ic_launcher);
//        builder.setTitle("I am the DRONE9000 app");
//        builder.setMessage(
//                "Swipe from the left to bring up a menu of accessible views.\n\n" +
//                        "Swipe from the right to bring up a quick-access menu to logged data.\n\n" +
//                        "You should pair me with a Sensordrone before going forward. This can be done from the Settings.\n\n" +
//                        "Items in the settings and the main screen need to be long-pressed"
//        );
//        builder.setPositiveButton("I won't remind you again.", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//            }
//        });
//        dialog = builder.create();
//        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialog) {
//                SharedPreferences.Editor editor = myPreferences.edit();
//                editor.putBoolean(PreferenceConstants.FIRST_BOOT, false);
//                editor.commit();
//            }
//        });
//        dialog.show();
//    }

    public void whatsNewDialog(String msg) {
        // Display a "What's New" Dialog if we've updated.
        // Get the current version code
        PackageInfo pinfo = null;
        try {
            pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            pinfo = null;
        }
        if (pinfo != null) {
            // Current Version
            final int currentVersionNumber = pinfo.versionCode;
            // Last Known Version
            int lastVersionNumber = myPreferences.getInt(PreferenceConstants.LAST_KNOWN_VERSION_CODE, -1);
            if (currentVersionNumber != lastVersionNumber) {

                Dialog dialog;
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("What's New");
                builder.setIcon(R.drawable.ic_launcher);
                builder.setMessage(msg);
                builder.setPositiveButton("Remind Me Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //
                    }
                });
                builder.setNegativeButton("Don't Remind Me Again", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SharedPreferences.Editor editor = myPreferences.edit();
                        editor.putInt(PreferenceConstants.LAST_KNOWN_VERSION_CODE, currentVersionNumber);
                        editor.commit();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        }
    }
}
