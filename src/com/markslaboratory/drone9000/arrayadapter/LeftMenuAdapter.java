package com.markslaboratory.drone9000.arrayadapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.markslaboratory.drone9000.R;

import java.util.List;

public class LeftMenuAdapter extends ArrayAdapter<String>{

    public LeftMenuAdapter(Context context, int resource) {
        super(context, resource);
    }

    public LeftMenuAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public LeftMenuAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
    }

    public LeftMenuAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public LeftMenuAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }

    public LeftMenuAdapter(Context context, int resource, int textViewResourceId, List<String> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View defaultView = super.getView(position, convertView, parent);
        if (position == 0) {
            defaultView.setBackgroundResource(R.color.menu_blue);
        }
        else if (position == 1) {
            defaultView.setBackgroundResource(R.color.menu_green);
        }
        else if (position == 2) {
            defaultView.setBackgroundResource(R.color.menu_red);
        }
        else if (position == 3) {
            defaultView.setBackgroundResource(R.color.menu_orange);
        }
        else if (position == 4) {
            defaultView.setBackgroundResource(R.color.menu_purple);
        }

        return defaultView;
    }
}
