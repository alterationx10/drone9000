package com.markslaboratory.drone9000.arrayadapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.markslaboratory.drone9000.R;

import java.util.List;

public class RightMenuAdapter extends ArrayAdapter<String>{

    public RightMenuAdapter(Context context, int resource) {
        super(context, resource);
    }

    public RightMenuAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public RightMenuAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
    }

    public RightMenuAdapter(Context context, int resource, int textViewResourceId, String[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public RightMenuAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }

    public RightMenuAdapter(Context context, int resource, int textViewResourceId, List<String> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View defaultView = super.getView(position, convertView, parent);

        defaultView.setBackgroundResource(R.color.menu_orange);

        return defaultView;
    }
}
