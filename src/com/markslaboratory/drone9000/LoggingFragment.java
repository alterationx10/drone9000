package com.markslaboratory.drone9000;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.j256.ormlite.dao.Dao;
import com.markslaboratory.drone9000.arrayadapter.LoggingAdapter;
import com.markslaboratory.drone9000.database.DatabaseHelper;
import com.markslaboratory.drone9000.logging.*;
import com.markslaboratory.drone9000.models.TemperatureLog;
import com.markslaboratory.drone9000.services.SensordroneService;

import java.sql.SQLException;

public class LoggingFragment extends ServiceFragment {

    ///
    DatabaseHelper db;
    Dao<TemperatureLog, Integer> dao;
    TemperatureLog log;
    ///

    ListView mainList;
    ArrayAdapter<String> listAdapter;

    public LoggingFragment(SensordroneService service) {
        super(service);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLEDColors(R.color.menu_orange);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.logging_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ///
        db = new DatabaseHelper(getActivity());
        try {
            dao = db.getTemperatureDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ///

        mainList = (ListView)getActivity().findViewById(R.id.logging_list);
        final String[] stuff = {
                "ADC",
                "Altitude",
                "Battery",
                "Capacitance",
                "CO",
                "Humidity",
                "IR Temperature",
                "Oxidizing Gas",
                "Pressure",
                "Reducing Gas",
                "R/G/B/C",
                "Temperature"
        };
        listAdapter = new LoggingAdapter(getActivity(), R.layout.logging_list_item, stuff);
        mainList.setAdapter(listAdapter);

        mainList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                Fragment fragment = null;
                if (mainList.getItemAtPosition(position).equals(stuff[0])) {
                    fragment = new ADCLogManagerFragment(droneService, new ADCLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[1])) {
                    fragment = new AltitudeLogManagerFragment(droneService, new AltitudeLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[2])) {
                    fragment = new BatteryLogManagerFragment(droneService, new BatteryLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[3])) {
                    fragment = new CapacitanceLogManagerFragment(droneService, new CapacitanceLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[4])) {
                    fragment = new COLogManagerFragment(droneService, new COLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[5])) {
                    fragment = new HumidityLogManagerFragment(droneService, new HumidityLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[6])) {
                    fragment = new IRLogManagerFragment(droneService, new IRLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[7])) {
                    fragment = new OxLogManagerFragment(droneService, new OxLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[8])) {
                    fragment = new PressureLogManagerFragment(droneService, new PressureLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[9])) {
                    fragment = new RedLogManagerFragment(droneService, new RedLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[10])) {
                    fragment = new RGBCLogManagerFragment(droneService, new RGBCLogFragment(droneService));
                }
                else if (mainList.getItemAtPosition(position).equals(stuff[11])) {
                    fragment = new TemperatureLogManagerFragment(droneService, new TemperatureLogFragment(droneService));
                }

                // Load the fragment
                if (fragment != null) {
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.content_frame, fragment);
                    // We want to add this one to the backstack
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                    fragmentTransaction.commit();
                }

                return true;
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}
