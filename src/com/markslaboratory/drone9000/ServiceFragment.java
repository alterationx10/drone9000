package com.markslaboratory.drone9000;


import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;
import com.markslaboratory.drone9000.services.SensordroneService;

public abstract class ServiceFragment extends Fragment {

    // Used for LED control... Set from XML resource
    int RED_HI;
    int RED_LOW;
    int GREEN_HI;
    int GREEN_LOW;
    int BLUE_HI;
    int BLUE_LOW;

    // Preferences
    SharedPreferences myPreferences;
    SharedPreferences.Editor prefEditor;

    // ASyncs
    public DataLogFetcher logFetcher;

    public void setLEDColors(int colorResource) {
        // Set the ON Colors
        RED_HI = Color.red(getResources().getColor(colorResource));
        GREEN_HI = Color.green(getResources().getColor(colorResource));
        BLUE_HI = Color.blue(getResources().getColor(colorResource));
        // Set the OFF colors to ~10% of ON
        RED_LOW = RED_HI / 10;
        GREEN_LOW = GREEN_HI / 10;
        BLUE_LOW = BLUE_HI / 10;
    }

    // Every class will access our SensordroneService
    public SensordroneService droneService;

    // We get passed a bound service with a Drone
    public ServiceFragment(SensordroneService service) {
        this.droneService = service;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Preferences
        myPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        prefEditor = myPreferences.edit();
    }

    @Override
    public void onResume() {
        super.onResume();
        droneService.sensorDrone.setLEDs(RED_HI, GREEN_HI, BLUE_HI);

    }

    @Override
    public void onPause() {
        super.onPause();
        droneService.sensorDrone.setLEDs(RED_LOW, GREEN_LOW, BLUE_LOW);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // Cancel any background tasks
        if (logFetcher != null && logFetcher.getStatus() == AsyncTask.Status.RUNNING) {
            logFetcher.cancel(true);
        }
    }

    public abstract class DataLogFetcher extends AsyncTask<Void,Void,Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Notify the user that we are getting data
            Toast.makeText(getActivity(),"Fetching Logged Data...", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // Do nothing if we've been canceled
            if (DataLogFetcher.this.isCancelled()) {
                return;
            }
        }
    }
}
