package com.markslaboratory.drone9000;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.markslaboratory.drone9000.services.SensordroneService;
import com.markslaboratory.drone9000.textview.BatteryTextView;
import com.sensorcon.sensordrone.DroneEventHandler;
import com.sensorcon.sensordrone.DroneEventObject;
import com.sensorcon.sensordrone.android.tools.DroneStreamer;

import java.util.Locale;

public class StatusFragment extends ServiceFragment implements DroneEventHandler{

    BatteryTextView batteryInfo;
    TextView firmwareInfo;
    TextView batteryPredictor;

    public StatusFragment(SensordroneService service) {
        super(service);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLEDColors(R.color.menu_orange);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.status_fragment, container, false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        batteryInfo = (BatteryTextView)getActivity().findViewById(R.id.status_btv_voltage);
        batteryInfo.setMyActivity(getActivity());
        batteryInfo.setMyDrone(droneService.sensorDrone);

        firmwareInfo = (TextView)getActivity().findViewById(R.id.status_tv_firmware);
        if (droneService.sensorDrone.isConnected) {
            String fw = "Firmware: " +
                    droneService.sensorDrone.hardwareVersion + "." +
                    droneService.sensorDrone.firmwareVersion + "." +
                    droneService.sensorDrone.firmwareRevision;
            firmwareInfo.setText(fw);
        }


        batteryPredictor = (TextView)getActivity().findViewById(R.id.status_tv_battery_remaining);


    }

    @Override
    public void onResume() {
        super.onResume();
        droneService.sensorDrone.registerDroneListener(this);
        droneService.sensorDrone.registerDroneListener(batteryInfo);
        // The BatteryTextView will keep measuring the voltage,
        // but we need to trigger it once
        if (droneService.sensorDrone.isConnected) {
            droneService.sensorDrone.measureBatteryVoltage();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        droneService.sensorDrone.unregisterDroneListener(this);
        droneService.sensorDrone.unregisterDroneListener(batteryInfo);
    }

    public String hoursLeft(float voltage) {
        if (voltage <= 3.67) {
            return "Less than 5 hours remaining!";
        }
        // For FW 1.1.3
        double slope = -2.019e-9; // V/ms
        // How many volts until 3.67?
        double voltsLeft = voltage - 3.67;
        double msLeft = -1 / slope * voltsLeft;
        double hoursLeft = msLeft / 1000 / 60 / 60;
        hoursLeft += 5; // The quick decay at the end
        return String.format(Locale.getDefault(),"Estimated Remaining Battery Life: %.0f Hours", hoursLeft);

    }

    @Override
    public void parseEvent(DroneEventObject droneEventObject) {
        if (droneEventObject.matches(DroneEventObject.droneEventType.BATTERY_VOLTAGE_MEASURED)) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    batteryPredictor.setText(hoursLeft(droneService.sensorDrone.batteryVoltage_Volts));
                }
            });
        }
    }
}
