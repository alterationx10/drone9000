Version 1.0.5

After ~a month of steady logging, I noticed that when viewing logged data, it could take a noticeable delay to update the UI. Therefore, I moved fetching of logged data to an async task to keep the UI more responsive.

Bug Fixes:
* Fixed a bug that could cause several views to overlay.

Version 1.0.4
Moved connecting to the Sensordrone to a Background thread. Should help with the UI responsiveness, and prevent ANR problems on some devices.

Version 1.0.3
Logged data selection is done from the right menu now. The main idea is that real-time information is accessed from the left menu, and logged information is accessed from the right menu.

Previously, selecting a sensor from the right menu would bring up a list of the logged data, and selecting 'Logged Data' from the left menu would bring up a list of sensors to select, and then go to a view/graph/send/delete view. Now, selecting a sensor from the right menu goes straight to the view/graph/send/delete view and the 'Logged Data' option from the left menu has been replaced with a 'Sensordrone Status' view.

Additions:
* Added a 'Sensordrone Status' view. There you can find out the Sensordrone: Battery Voltage, Firmware Version,  and Estimated Remaining Battery Life in hours.

Bug Fixes:
* Fixed the wrong month number was being displayed while viewing the logged data for a sensor (was showing 1 month behind).

Future plans:
* Color themes: I plan to have a 'Light', 'Dark', 'Blinding' (that's the current one), and 'Custom' color theme for the app.